const PROGRAM_TYPE = {
    SOFTWARE:1,
    HARDWARE:2
}

const PROGRAM_TYPE_NAME = {
    SOFTWARE:'Software',
    HARDWARE:'Hardware'
}

const TRAINING_STATUS = {
    STILL_IN_TRAINING:'Still in training',
    GRADUATED:'Graduated',
    NOT_GRADUATE:'Did not graduate'
}

const STATUS = {
    HOLD:'hold', 
    DECEASED:'deceased', 
    RESIGNED:'resigned', 
    ENROLLED:'enrolled',
    PAYROLL_EXEMPT:'payroll_exempt',
    EXITED:'exited'
}

module.exports = {
    PROGRAM_TYPE,
    PROGRAM_TYPE_NAME,
    TRAINING_STATUS,
    STATUS
}