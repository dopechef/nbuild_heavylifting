const redis = require("redis");

class RedisHelper {
    constructor () {
        this.redisClient = redis.createClient({
            host: process.env.REDIS_CLUSTER_HOST
        });
        this.redisClient.on("error", function (err) {
            console.log("Error " + err);
        });
        this.redisClient.on('connect', () => {
            console.log('Connected to Redis!');
        });
    }

    redisInstance () {
        return this.redisClient
    }

    set (key, value) {
        return new Promise((resolve, reject)=>{
            this.redisClient.set(key, value, (err, resp)=>{
                if(err){
                    return reject(err)
                }
                return resolve(resp)
            })
        })
    }

    get (key) {
        return new Promise((resolve, reject)=>{
            this.redisClient.get(key, (err, resp)=>{
                if(err){
                    return reject(err)
                }
                return resolve(resp)
            })
        })
    }
}

module.exports = {
    createRedisKey: function (key) {
        return 'Nbuild$' + key;
    },
    redisInstane: new RedisHelper()
}