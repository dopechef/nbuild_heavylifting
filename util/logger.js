const request = require('request');
const conn = require('../config/dbNbuild');

const Log = require('../app/models/log');

module.exports = {
  log(id, srcip, event, title, description) {
    const log = new Log();
    log.event = event;
    log.title = title;
    log.src_ip = srcip;
    log.description = description;
    log.admin = id;


    request({ method: 'GET', url: `https://tools.keycdn.com/geo.json?host=${srcip}` }, (err, res, body) => {
      console.log(body);

      const json = JSON.parse(body);

      if (json.status === 'success') {
        console.log('success');
        log.geo = json.data.geo;
      }

      console.log(log);

      log.save((errr) => {
        if (errr) {
          console.log(err);
        }
      });
    });
  },
};
