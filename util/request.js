// const request = require('request');
const requestPromise = require('request-promise-native');


module.exports.postToURL = async function postToURL(requestOptions) {
  console.log('called');
  const response = await requestPromise(requestOptions);
  console.log(response);
  return response;
};

module.exports.getFromURL = async function getFromURL(requestOptions) {
  const response = await requestPromise(requestOptions);
  return response;
};
