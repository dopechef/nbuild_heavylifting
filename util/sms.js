const http = require('http');
const querystring = require('querystring');
const config = require('../config/config.js');
const https = require('https');
const request = require('../util/request.js');

module.exports = {
  async send(phone_number, message) {
    phone_number = phone_number.replace('+', '');

    console.log(`SMS sent to: ${phone_number}`);

    const body = {
      mobile: phone_number,
      sender: 'NPVN',
      msg: message,
    };

    const requestOptions = {
      method: 'POST',
      uri: 'http://api.sms.softcom.ng',
      body,
      json: true, // Automatically stringifies the body to JSON
    };

    try {
      const response = await request.postToURL(requestOptions);
      console.log(response);
      // if (response.data[0].score && response.data[0].score > 0.5) {
      if (response.success) {
        console.log('Sent successfully');
      }
      console.log('Message not sent successfully');
    } catch (err) {
      console.error(err);
    }

    /*
    const body = JSON.stringify({
      SMS: {
        auth: {
          username: 'services@softcom.ng',
          apikey: '4d0f7ae2cfef4d0e1d59941c9f11cb07225ac991',
        },
        message: {
          sender: 'NPVN Token',
          messagetext: message,
          flash: '0',
        },
        recipients:
                {
                  gsm: [
                    {
                      msidn: phone_number,
                      msgid: '1',
                    },
                  ],
                },
      },
    });

    const requestOptions = {
      method: 'POST',
      uri: 'http://api.ebulksms.com:8080/sendsms.json',
      body,
      json: true, // Automatically stringifies the body to JSON
    };

    try {
      const response = await request.postToURL(requestOptions);
      console.log(response);
      // if (response.data[0].score && response.data[0].score > 0.5) {
      if (response.response.status === 'SUCCESS') {
        console.log('Sent successfully');
      }
      console.log('Message not sent successfully');
    } catch (err) {
      console.error(err);
    } */
  },
  sendVolunteer(phone_number, message) {
    // phone_number = phone_number.replace('+', '');
    // remove the 0 from the beginning of the phone number
    phone_number = phone_number.substr(1);
    // Add 234 to the beginning
    phone_number = `234${phone_number}`;

    console.log(`SMS sent to: ${phone_number}`);

    const body = JSON.stringify({
      SMS: {
        auth: {
          username: 'services@softcom.ng',
          apikey: '4d0f7ae2cfef4d0e1d59941c9f11cb07225ac991',
        },
        message: {
          sender: 'NPVN',
          messagetext: message,
          flash: '0',
        },
        recipients:
                {
                  gsm: [
                    {
                      msidn: phone_number,
                      msgid: '1',
                    },
                  ],
                },
      },
    });

    const httpreq = http.request(config.smshttpoptions, (response) => {
      let str = '';
      response.on('data', (chunk) => {
        str += chunk;
      });

      response.on('end', () => {
        console.log(str);
        // sample success message: {"response":{"status":"SUCCESS","totalsent":1,"cost":1}}
        try {
          const parsed = JSON.parse(str);
          console.log(parsed); // JSON Object
        } catch (err) {
          console.error('Cant parse response');
          console.error(str);
        }

        // console.log(parsed.response.status);    //string
        // console.log(parsed.response.cost);  //integer
        // console.log(parsed.response.totalsent); //integer
      });
    });

    httpreq.end(body);
  },
  sendAfricaIsTalking(mobile, message) {
    const username = 'eyowo';
    const apikey = '2d16848ee2c2b430bbb6a656784baabb77f67fd12103eb07addc483c59fb893d';

    // Add + character
    const to = mobile.replace('0', '+234');

    // Define the recipient numbers in a comma separated string
    // Numbers should be in international format as shown
    // const to = '+2348066953623,+2349092345679';

    // And of course we want our recipients to know what we really do
    // const message = "Your Eyowo validation code is 5678";

    // Build the post string from an object
    const postData = querystring.stringify({
      username,
      to,
      message,
      from: 'NPVN',
    });

    const postOptions = {
      host: 'api.africastalking.com',
      path: '/version1/messaging',
      method: 'POST',
      rejectUnauthorized: false,
      requestCert: true,
      agent: false,

      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': postData.length,
        Accept: 'application/json',
        apikey,
      },
    };

    const postReq = https.request(postOptions, (res) => {
      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        console.log(chunk);
        const jsObject = JSON.parse(chunk);
        const recipients = jsObject.SMSMessageData.Recipients;
        if (recipients.length > 0) {
          for (let i = 0; i < recipients.length; i += 1) {
            let logStr = `number=${recipients[i].number}`;
            logStr += `;cost=${recipients[i].cost}`;
            logStr += `;status=${recipients[i].status}`; // status is either "Success" or "error message"
            console.log(logStr);
          }
        } else {
          console.log(`Error while sending: ${jsObject.SMSMessageData.Message}`);
        }
      });
    });
    // Add post parameters to the http request
    postReq.write(postData);
    postReq.end();
  },
  async sendSMSLive247(mobile, message) {
    const to = mobile.replace('0', '234');
    const requestOptions = {
      method: 'GET',
      uri: `http://www.smslive247.com/http/index.aspx?cmd=sendmsg&sessionid=786223bd-4605-4cf7-b39f-82d388c28738&message=${message}&sender=NPVN&sendto=${to}&msgtype=0`,
    };

    try {
      const response = await request.getFromURL(requestOptions);
      console.log(response);
    } catch (err) {
      console.error(err);
    }
  },
};

