const nodemailer = require('nodemailer');
const striptags = require('striptags');
const Mailgun = require('mailgun-js');

const config = require('../config/config.js');

module.exports = {
  // attachments = array of object attachments. more info: https://nodemailer.com/using-attachments/
  // eslint-disable-next-line
  send(recipient_email, subject, htmlmessage, attachments, replyTo) {
    const mailtransporter = nodemailer.createTransport(config.smtpConfig);

    const mailOptions = {
      from: '"Nbuild"', // sender address
      to: recipient_email, // list of receivers
      subject, // Subject line
      text: striptags(htmlmessage), // plaintext body
    };

    if (replyTo !== undefined) {
      mailOptions.replyTo = replyTo;
    }

    if (attachments !== undefined) {
      mailOptions.attachments = attachments;
    }

    // send mail with defined transport object
    mailtransporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(`***MAIL*** Problem sending mail ${error}`);
        return;
      }
      console.log(`***MAIL*** Message sent: ${info.response}`);
    });
  },
  // bcc is an array of emal addresses
  sendBcc(bcc, subject, htmlmessage, attachments, replyTo) {
    const mailtransporter = nodemailer.createTransport(config.smtpConfig);

    const mailOptions = {
      from: '"NPVN" <notifications@npvn.ng>', // sender address
      bcc, // list of receivers
      subject, // Subject line
      text: striptags(htmlmessage), // plaintext body
      html: htmlmessage,
    };

    if (replyTo !== undefined) {
      mailOptions.replyTo = replyTo;
    }

    if (attachments !== undefined) {
      mailOptions.attachments = attachments;
    }

    // send mail with defined transport object
    mailtransporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log(`***MAIL*** Problem sending mail ${error}`);
        return;
      }
      console.log(`***MAIL*** Message sent: ${info.response}`);
    });
  },
  sendMailGun(recipientEmail, subject, htmlmessage, attachments, replyTo) {
    // We pass the api_key and domain to the wrapper, or it won't be able to identify + send emails
    const mailgun = new Mailgun({
      apiKey: 'key-4fb47156ee9fbb64719c01b6fd80a189',
      domain: 'mailgun.npvn.ng',
    });

    const data = {
      // Specify email data
      from: 'no-reply@npvn.ng',
      // The email to contact
      to: recipientEmail,
      // Subject and text data
      subject,
      html: htmlmessage,
    };

    // Invokes the method to send emails given the above data with the helper library
    mailgun.messages().send(data, (err, body) => {
      // If there is an error, render the error page
      if (err) {
        console.log('got an error: ', err);
      } else {
        // Here "submitted.jade" is the view file for this landing page
        // We pass the variable "email" from the url parameter in an object rendered by Jade
        console.log(body);
      }
    });
  },
};
