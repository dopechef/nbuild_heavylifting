require('dotenv').config();

const cluster = require('cluster');
const numWorkers = require('os').cpus().length;
const express = require('express');
const routes = require('./app/index');
const mws = require('./config/middlewares');
//const dbConnection = require('./config/dbNbuild');
const app = express();
//require('./workers/export-table');

app.use(mws);
app.use(routes.volunteer);
app.use(routes.payrolls);
app.use(routes.payments);
app.use(routes.centres);
app.use(routes.attendance);
app.use(routes.exportDocs);

// connect to db
//dbConnection();
// for it to have many clusters
if (process.env.ENVIRONMENT === 'production' && cluster.isMaster) {
  console.log(`Master cluster setting up ${numWorkers} workers...`);

  for (let i = 0; i < numWorkers;) {
    cluster.fork();
    i += 1;
  }
  cluster.on('online', (worker) => {
    console.log(`Worker ${worker.process.pid} is online`);
  });

  cluster.on('exit', (worker, code, signal) => {
    console.log(`Worker ${worker.process.pid} died with code: ${code}, and signal: ${signal}`);
    console.log('Starting a new worker');
    cluster.fork();
  });
} else {
  const port = process.env.ENVIRONMENT === 'staging' ?  process.env.PORT || 1337 : '3000';
  app.listen(port, () => {
    console.log(`app running on port: ${port}`);
  });
};