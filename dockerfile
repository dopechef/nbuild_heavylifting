FROM node:carbon
# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# COPY package.json .
# For npm@5 or later, copy package-lock.json as well
COPY package.json package-lock.json ./

RUN npm install

ENV MONGO_URI=mongodb://dev0-shard-00-00-x4oy5.mongodb.net:27017,dev0-shard-00-02-x4oy5.mongodb.net:27017/nbuild?replicaSet=Dev0-shard-0
ENV NTECH_MONGO_URI=mongodb://dev0-shard-00-00-x4oy5.mongodb.net:27017,dev0-shard-00-02-x4oy5.mongodb.net:27017/ntech?replicaSet=Dev0-shard-0
ENV ENVIRONMENT=production
ENV FEDERATION_SERVER=https://npower-federation-server.herokuapp.com/v1/npower-verify/verify-user/
ENV RABBIT_MQ = amqp://twxrlnge:dNeW80avJ439XdGC75vsHe7Zcp0WV7gl@raven.rmq.cloudamqp.com/twxrlnge

# Bundle app source
COPY . .

#port
EXPOSE 3000

CMD [ "node", "server.js" ]