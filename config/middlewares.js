const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const router = express.Router();

router.use(cors());
router.use(helmet());
router.use(morgan('combined'));
router.use(bodyparser.urlencoded({ extended: true }));
router.use(bodyparser.json());

module.exports = router;
