module.exports = {
  port: process.env.PORT || 8080,
  secret: 'P@ssw0rd123$',
  tokenSecret: 'P@ssw0rd123$',
  // NodeMailer config
  smtpConfig: {
    service: 'gmail',
    auth: {
      user: 'no-reply@softcom.ng',
      pass: 'P@ssw0rd123$',
    },
  },
  smshttpoptions: {
    host: 'api.ebulksms.com',
    path: '/sendsms.json',
    port: '8080',
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
  },
};
