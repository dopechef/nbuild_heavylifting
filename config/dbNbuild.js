const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const options = {}
options['useNewUrlParser'] = true
if(process.env.ENVIRONMENT === 'production'){
  options['auth'] = { authdb: 'admin' }
  options['user'] = 'dev0'
  options['pass'] = '8qnx6uGmVnNeANln'
  options['ssl'] = true
  // connectTimeoutMS: 1440 * 60 * 60, // Give up initial connection after 10 seconds
  // socketTimeoutMS: 45000 * 60 * 60,
}

const mongoUri = process.env.MONGO_URI
const ntechMongoUri = process.env.NTECH_MONGO_URI
//const mongoUri = 'mongodb://dev0-shard-00-00-x4oy5.mongodb.net:27017,dev0-shard-00-02-x4oy5.mongodb.net:27017/nbuild?replicaSet=Dev0-shard-0'

const nbuildConnection = mongoose.createConnection(
  mongoUri,
  options,
  (err) => {
    if (err) throw err;
    console.log('db connected.');
  },
);

const ntechConnection = mongoose.createConnection(
  ntechMongoUri,
  options,
  (err) => {
    if (err) throw err;
    console.log('db connected.');
  },
);

module.exports = {
  nbuildConnection,
  ntechConnection
};
