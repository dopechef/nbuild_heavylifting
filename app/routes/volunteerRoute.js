const express = require('express');
const multer = require('multer');
const VolunteerCtrl = require('../controllers/volunteerCtrl');
//const createVolunteer = require('../controllers/volunteers/create');

const api = express.Router();
const upload = multer({ dest: './uploads' });

api.post(
  '/v1/nbuild/volunteer/updateBodies', 
  upload.single('excel_file'),
  VolunteerCtrl.addRegulatoryBodies,
);

api.post(
  '/v1/nbuild/volunteer/updateexcel',
  upload.single('excel_file'),
  VolunteerCtrl.updateVolunteer,
);

/* api.post(
  '/v1/ntech/volunteers/create-bulk',
  VolunteerCtrl.createBatchNtechVolunteers
) */

api.post(
  '/v1/ntech/volunteers/upload',
  upload.single('excel_file'),
  VolunteerCtrl.batchCreateNtechVolunteers
)

api.post(
  '/v1/ntech/volunteers/confirm_exists',
  upload.single('excel_file'),
  VolunteerCtrl.confirmVolunteerExists
)

api.post(
  '/v1/ntech/volunteers/update_reg_status',
  upload.single('excel_file'),
  VolunteerCtrl.updateRegStatus
) 

api.post(
  '/v1/export-table',
  upload.single('excel_file'),
  VolunteerCtrl.updateRegStatus
) 

module.exports = api;
