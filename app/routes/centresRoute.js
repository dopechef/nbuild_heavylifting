const express = require('express');
const multer = require('multer');
const CentresCtrl = require('../controllers/centresCtrl');

const api = express.Router();
const upload = multer({ dest: './uploads' });

api.post(
  '/v1/nbuild/centres/updateBodies',
  upload.single('excel_file'),
  CentresCtrl.addRegulatoryBodies,
);

api.post(
  '/v1/nbuild/centreadmin/upload',
  upload.single('excel_file'),
  CentresCtrl.uploadCentreAdmin,
);

api.post(
  '/v1/nbuild/centre/upload',
  upload.single('excel_file'),
  CentresCtrl.uploadCentres,
);

api.post(
  '/v1/nbuild/centre/delete-admins',
  CentresCtrl.deleteAdmins
);

api.post(
  '/v1/ntech/centre/update-quota',
  upload.single('excel_file'),
  CentresCtrl.updateCentreQuota
);

api.post(
  '/v1/ntech/centre/update-batch',
  upload.single('excel_file'),
  CentresCtrl.updateBatch
);

module.exports = api;
