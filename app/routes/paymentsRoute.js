const express = require('express');
const multer = require('multer');
const PaymentsCtrl = require('../controllers/paymentsCtrl');

const api = express.Router();
const upload = multer({ dest: './uploads' });

const nbuildVolunteer = require('../models/volunteer');
const ntechVolunteer = require('../models/ntechers');

//  upload Payment file  Note:excel_file is the fieldname for the file upload
api.post(
  '/v1/nbuild/admin/payments/uploads',
  upload.single('excel_file'),
  (req, res) => PaymentsCtrl.uploadPayments(req, res, nbuildVolunteer),
);

api.post(
  '/v1/ntech/admin/payments/uploads',
  upload.single('excel_file'),
  (req, res) => PaymentsCtrl.uploadPayments(req, res, ntechVolunteer),
);

module.exports = api;
