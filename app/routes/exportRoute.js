const express = require('express');
const api = express.Router();
const ExportCtrl = require('../controllers/export/volunteer');
const NtechExportCtrl = require('../controllers/export/ntechers');

api.get('/v1/nbuild/export',ExportCtrl);
api.get('/v1/ntech/export',NtechExportCtrl);

module.exports = api;