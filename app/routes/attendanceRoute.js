const express = require('express');
const multer = require('multer');
const AttendanceCtrl = require('../controllers/attendanceCtrl');

const api = express.Router();
const upload = multer({ dest: './uploads' });

api.post(
  '/v1/nbuild/cadmin/attendance',
  upload.single('excel_file'),
  AttendanceCtrl.generateAttendance,
);

api.post(
  '/v1/ntech/cadmin/attendance',
  upload.single('excel_file'),
  AttendanceCtrl.generateNtechAttendance,
);

module.exports = api;
