const express = require('express');
const multer = require('multer');
const PayrollsCtrl = require('../controllers/payrollsCtrl');

const api = express.Router();
const upload = multer({ dest: './uploads' });

//  upload Payment file  Note:excel_file is the fieldname for the file upload
api.post(
  '/v1/nbuild/admin/payrolls/authorize',
  upload.single('excel_file'),
  PayrollsCtrl.authorizePayroll,
);

module.exports = api;
