const mongoose = require('mongoose');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const PaymentSchema = new Schema({
  volunteer_id: { type: Schema.Types.ObjectId, required: true, ref: 'Volunteer' },
  date: { type: Date, required: true },
  amount: { type: Number, required: true },
  type: {
    type: String,
    required: true,
    enum: [
      'Main',
      'Supplementary',
      'Backlog 1',
      'Backlog 2',
      'Backlog 3',
      'Backlog 4',
      'Backlog 5',
      'Backlog 6',
      'Backlog 7',
      'Backlog 8',
      'Backlog 9',
      'Backlog 10',
    ],
  },
  status: {
    type: String,
    reqired: true,
    enum: ['Paid', 'Invalid', 'Unpaid'],
  },
  batch: { type: Number, required: false },
});

PaymentSchema.index({ date: 1, volunteer_id: 1 }, { unique: true });

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('payment', PaymentSchema);
// module.exports = PaymentSchema;
