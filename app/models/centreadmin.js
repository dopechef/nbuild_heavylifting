// grab the packages that we need for the admin model
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')
// center admin schema
const CentreAdminSchema = new Schema({
  username: { type: String, required: false, index: { unique: true } },
  fullname: { type: String, required: false },
  mobile1: { type: String, required: false },
  mobile2: { type: String, required: false },
  email: { type: String, required: false },
  role: {
    type: mongoose.SchemaTypes.ObjectId,
    required: true,
  },
  password: { type: String, select: true },
  centre_id: { type: mongoose.SchemaTypes.ObjectId, required: true },
  date_created: { type: Date, default: Date.now },
  last_login: Date,
  first_login: { type: Boolean, default: true },
});

module.exports = nbuildConnection.model('centreadmin', CentreAdminSchema);
