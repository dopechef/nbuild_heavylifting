const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const { Schema } = mongoose;
const {REGULATORY_BODIES} = require('../../util/enum.utils');
const {nbuildConnection} = require('../../config/dbNbuild')

const NBuildCentreSchema = new Schema({
  id: { type: Number, required: false },
  'TRAINEES NC': { type: String, required: false },
  STATE: { type: String, required: false },
  CENTRE: { type: String, required: false, unique:true },
  LGA: { type: String, required: false },
  AUTOMOBILE: { type: Number, required: false },
  AUTOMOBILE_1: { type: Number, required: false },
  AUTOMOBILE_2: { type: Number, required: false },
  'CARPENTRY & JOINERY': { type: Number, required: false },
  'CARPENTRY & JOINERY_1': { type: Number, required: false },
  'CARPENTRY & JOINERY_2': { type: Number, required: false },
  ELECTRICAL: { type: Number, required: false },
  ELECTRICAL_1: { type: Number, required: false },
  ELECTRICAL_2: { type: Number, required: false },
  MASONRY: { type: Number, required: false },
  MASONRY_1: { type: Number, required: false },
  MASONRY_2: { type: Number, required: false },
  'PAINTING & DECORATING': { type: Number, required: false },
  'PAINTING & DECORATING_1': { type: Number, required: false },
  'PAINTING & DECORATING_2': { type: Number, required: false },
  'AGRIC TECHNOLOGY': { type: Number, required: false },
  'AGRIC TECHNOLOGY_1': { type: Number, required: false },
  'AGRIC TECHNOLOGY_2': { type: Number, required: false },
  PLUMBING: { type: Number, required: false },
  PLUMBING_1: { type: Number, required: false },
  PLUMBING_2: { type: Number, required: false },
  WELDING: { type: Number, required: false },
  WELDING_1: { type: Number, required: false },
  WELDING_2: { type: Number, required: false },
  HOSPITALITY: { type: Number, required: false },
  HOSPITALITY_1: { type: Number, required: false },
  HOSPITALITY_2: { type: Number, required: false },
  BATCH:{type: Schema.Types.Array },
  automobile_pending: { type: Number, required: false },
  carpentry_pending: { type: Number, required: false },
  electrical_pending: { type: Number, required: false },
  masonry_pending: { type: Number, required: false },
  painting_pending: { type: Number, required: false },
  plumbing_pending: { type: Number, required: false },
  welding_pending: { type: Number, required: false },
  status: {
    type: String,
    required: true,
    default: 'active',
    enum: ['active', 'inactive'],
  },
  brand:{
    type: Schema.Types.Array
  },
  regulatory_body:{ 
    type:Schema.Types.Array, 
    enum:[REGULATORY_BODIES.NDDC, REGULATORY_BODIES.CORBON] 
  }// nddc = 1, corbon = 2
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('nbuildcentre', NBuildCentreSchema);
// module.exports = NBuildCentreSchema;