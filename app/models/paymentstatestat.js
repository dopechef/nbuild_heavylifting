const mongoose = require('mongoose');
const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const PaymentStateStatSchema = Schema({
  state_id: {
    type: String,
    required: false,
    enum: [
      '1',
      '2',
      '3',
      '4',
      '5',
      '6',
      '7',
      '8',
      '9',
      '10',
      '11',
      '12',
      '13',
      '14',
      '15',
      '16',
      '17',
      '18',
      '19',
      '20',
      '21',
      '22',
      '23',
      '24',
      '25',
      '26',
      '27',
      '28',
      '29',
      '30',
      '31',
      '32',
      '33',
      '34',
      '35',
      '36',
      '37',
    ],
  },
  state: { type: String, required: true },
  date: { type: Date, required: true },
  amount: { type: Number, required: true },
  count: { type: Number, required: true },
});

PaymentStateStatSchema.index({ date: 1, state: 1 }, { unique: true });

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('paymentstatestat', PaymentStateStatSchema);
// module.exports = PaymentStateStatSchema;
