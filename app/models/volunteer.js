const mongoose = require('mongoose');
const {REGULATORY_BODIES, TRADES_ARRAY} = require('../../util/enum.utils');
const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const VolunteerSchema = new Schema({
  sn: { type: String, required: false },
  fname: { type: String, required: false },
  mname: { type: String, required: false },
  lname: { type: String, required: false },
  address: { type: String, required: false },
  state: { type: String, required: false },
  password: { type: String, required: false },
  gender: { type: String, required: false, enum: ['M', 'F'] },
  birth_day: { type: String, required: false },
  birth_month: { type: String, required: false },
  birth_year: { type: String, required: false },
  phone: { type: String, required: false, index: true, unique:true },
  email: { type: String, required: false, unique:true },
  bvn: { type: String, required: false, index: true, unique:true },
  lga: { type: String, required: false },
  bank_name: { type: String, required: false },
  nuban: { type: String, required: false, index: true, unique:true },
  nibss_validated: { type: Boolean, required: true, default: false },
  nibss_validated_reason: {type: String},
  interest1: { type: String, required: false },
  location1: { type: String, required: false },
  created_at: { type: String, required: false },
  interest2: { type: String, required: false },
  location2: { type: String, required: false },
  interest3: { type: String, required: false },
  location3: { type: String, required: false },
  trade_allocation: { type: String, required: false, enum:TRADES_ARRAY },
  centre_allocated: { type: Boolean, required: false },
  centre_allocation: { type: String, required: false },
  centre_allocation_reason: { type: String, required: false },
  centre_id: { type: Schema.Types.ObjectId, required: false },
  status: {
    index: true,
    type: String,
    required: false,
    enum: ['hold', 'deceased', 'resigned', 'enrolled', 'payroll_exempt', 'exited'],
  },
  program_status: {
    index: true,
    type: String,
    required: false,
    enum: ['Verified', 'Deployed', 'Registered', 'Employed'],
  },
  training_status: {
    type: String,
    required: false,
    enum: ['Still in training', 'Graduated', 'Did not graduate'],
    default: 'Still in training',
  },
  verified: { type: Boolean, required: true, default: false },
  batch: { type: Number, required: true },
  deployed: { type: Boolean, required: true, default: false },
  registered: { type: Boolean, required: true, default: false },
  employed: { type: Boolean, required: true, default: false },
  special_registration: { type: Boolean, required: true, default: false },
  employment_date: { type: Date, required: false },
  employer: { type: String, required: false },
  // The following 4 fields are images that reference fs and chunks in mongodb
  profile_pic: { type: Schema.Types.ObjectId, required: false },
  bvn_slip: { type: Schema.Types.ObjectId, required: false },
  registration_form: { type: Schema.Types.ObjectId, required: false },
  identification: { type: Schema.Types.ObjectId, required: false },
  updated: { type: Boolean, default: false, required: true },
  work_placement:{type:String},
  work_placement_letter:{type:String},
  work_placement_address:{type:String},
  id_card: {type:String},
  profile_picture:{type:String},
  geozone:{type:String},
  regulatory_body:{ type:Schema.Types.Array },
  ward: {type: String }
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('volunteer', VolunteerSchema)