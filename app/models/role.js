const mongoose = require('mongoose');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const RoleSchema = new Schema({
  label: { type: String, required: true },
  permissions: [{ type: String }],
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('role', RoleSchema);
