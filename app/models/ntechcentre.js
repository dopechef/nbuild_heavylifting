const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const {PROGRAM_TYPE} = require('../../util/constants.helper');

const { Schema } = mongoose;
const {ntechConnection} = require('../../config/dbNbuild')

const NTechCentreSchema = new Schema({
  //state: { type: String, required: true },
  //spread: { type: String, required: true },
  batch:{type: Schema.Types.Array, 'default':[1] },
  status: {
    type: String,
    required: true,
    default: 'active',
    enum: ['active', 'inactive'],
  },
  program: { type: String, 
    required: false , 
    enum:[PROGRAM_TYPE.SOFTWARE, PROGRAM_TYPE.HARDWARE], 
    index:true
  },//1 = software, 2 = hardware
 capacity:{type:Number, required:true},
 geozone: {type:String, required:true },
 centre_name:{type:String, required:true},
 centre_state:{type:String, required:true},
 centre_lga:{type:String, required:true},
 spread:{type:String, required:true},
 total_capacity:{type:Number, required:true, default:1000},
 centre_id:{type:Schema.Types.ObjectId},
 //center_info:{ type: Schema.Types.ObjectId, ref: 'ntechadmin' },
 sn:{type:Number},
 state:{type:String, required:true}
});

NTechCentreSchema.virtual('program_name').get(()=>{
  return this.program != null && this.program === PROGRAM_TYPE.SOFTWARE ? 'Software':'Hardware'
})

// create the model for users and expose it to our app
module.exports = ntechConnection.model('ntechcentre', NTechCentreSchema);
// module.exports = NBuildCentreSchema;