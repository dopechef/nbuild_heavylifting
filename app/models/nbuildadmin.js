// grab the packages that we need for the admin model
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
mongoose.Promise = require('bluebird');
const {nbuildConnection} = require('../../config/dbNbuild')

const { Schema } = mongoose;

// nbuild admin schema
const NBuildAdminSchema = new Schema({
  name: { fname: { type: String, required: true }, lname: { type: String, required: true } },
  email: { type: String, required: true, index: { unique: true } },
  phone: { type: String, required: true, index: { unique: true } },
  password: { type: String, select: true },
  role: { type: Schema.Types.ObjectId, required: true },
  date_created: { type: Date, default: Date.now },
  last_login: Date,
});

// hash the password before the admin is saved
NBuildAdminSchema.pre('save', (next) => {
  const admin = this;

  // generate the hash
  return bcrypt.hash(admin.password, null, null, (err, hash) => {
    if (err) return next(err);

    // change the password to the hashed version
    admin.password = hash;
    return next();
  });
});

// method to compare a given password with the database hash
NBuildAdminSchema.methods.comparePassword = (password) => {
  const admin = this;
  return bcrypt.compareSync(password, admin.password);
};

// return the model
module.exports = nbuildConnection.model('nbuildadmin', NBuildAdminSchema);

// return the schema
