const mongoose = require('mongoose');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const PermissionSchema = new Schema({
  label: { type: String, required: true },
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('permission', PermissionSchema);
// module.exports = PermissionSchema;
