// grab the packages that we need for the admin model
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const {nbuildConnection, ntechConnection} = require('../../config/dbNbuild')
const {PROGRAM_TYPE, TRAINING_STATUS, STATUS} = require('../../util/constants.helper');

const { Schema } = mongoose;
const baseSchema = {
  volunteer_id: { type: Schema.Types.ObjectId, required: true, unique:true },
  attendance_id: { type: Schema.Types.ObjectId, required: true, unique:true },
  fname: { type: String, required: false },
  mname: { type: String, required: false },
  lname: { type: String, required: false },
  phone: { type: String, required: false },
  centre_id: { type: Schema.Types.ObjectId, required: true },
  percentage: { type: Number, default: 0, required: true },
  days: { type: [Number], required: false },
  date_created: { type: Date, default: Date.now },
  date_marked: { type:Date, required: true },
  batch: {type: Number, required:true},
}
Object.freeze(baseSchema)

const nbuildSchema = {
  trade_allocation: {
    type: String,
    enum: [
      'MASONRY',
      'AUTOMOBILE',
      'ELECTRICAL',
      'PAINTING & DECORATING',
      'PLUMBING',
      'WELDING',
      'CARPENTRY & JOINERY',
    ],
    required: true,
  }
}
Object.assign(nbuildSchema, baseSchema)
const ntechSchema = {
  program: { 
    type: String, 
    required: false , 
    enum:[PROGRAM_TYPE.SOFTWARE, PROGRAM_TYPE.HARDWARE]
  },//1 = software, 2 = hardware
}
Object.assign(ntechSchema, baseSchema)
// center admin schema

/*nbuildSchema.index({ volunteer_id: 1, attendance_id: 1 }, { unique: true });
ntechSchema.index({ volunteer_id: 1, attendance_id: 1 }, { unique: true });*/

module.exports = {
  nbuild: nbuildConnection.model('volunteerattendance', nbuildSchema),
  ntech: ntechConnection.model('volunteerattendance', ntechSchema)
}
