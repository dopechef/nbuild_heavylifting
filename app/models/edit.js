// grab the packages that we need for the admin model
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

const EditSchema = new Schema({
  date: { type: Date, default: Date.now },
  requestor_type: { type: String, required: true, enum: ['volunteer', 'admin'] },
  // This is the ID of the admin or volunteer requesting the change
  requestor: { type: Schema.Types.ObjectId, required: true },
  // This tells us the ID of the object being edited
  object_id: { type: Schema.Types.ObjectId, required: true },
  // This tells us the type of object being edited
  object_type: { type: String, required: true, enum: ['volunteer'] },
  status: { type: String, required: true, enum: ['pending', 'approved', 'disapproved'] },
  reason: { type: String, required: true },
  old: {
    firstname: { type: String, required: false },
    middlename: { type: String, required: false },
    surname: { type: String, required: false },
    phone: { type: String, required: false },
    email: { type: String, required: false },
    dob: { type: Date, required: false },
    address: { type: String, required: false },
    bankname: { type: String, required: false },
    bankaccount: { type: String, required: false },
    bankbvn: { type: String, required: false },
    accountname: { type: String, required: false },
    gender: {
      type: String,
      required: false,
      enum: ['male', 'female'],
    },
    status: {
      type: String,
      required: false,
      enum: ['hold', 'deceased', 'resigned', 'enrolled', 'payroll_exempt', 'exited', 'unpaid'],
    },
    program_status: {
      type: String,
      required: false,
      enum: ['selected', 'qualified', 'nibss_verified'],
    },
    device_status: {
      type: String,
      required: false,
      enum: ['shipped', 'processing', 'delivered', 'unassigned', 'hold'],
    },
    verification_date: { type: Date, required: false },
    device_batch: { type: String, required: false },
    residence_lga: { type: String, required: false },
    residence_state: { type: String, required: false },
  },
  new: {
    firstname: { type: String, required: false },
    middlename: { type: String, required: false },
    surname: { type: String, required: false },
    phone: { type: String, required: false },
    email: { type: String, required: false },
    dob: { type: Date, required: false },
    address: { type: String, required: false },
    bankname: { type: String, required: false },
    bankaccount: { type: String, required: false },
    bankbvn: { type: String, required: false },
    accountname: { type: String, required: false },
    gender: {
      type: String,
      required: false,
      enum: ['male', 'female'],
    },
    status: {
      type: String,
      required: false,
      enum: ['hold', 'deceased', 'resigned', 'enrolled', 'payroll_exempt', 'exited', 'unpaid'],
    },
    program_status: {
      type: String,
      required: false,
      enum: ['selected', 'qualified', 'nibss_verified'],
    },
    device_status: {
      type: String,
      required: false,
      enum: ['shipped', 'processing', 'delivered', 'unassigned', 'hold'],
    },
    verification_date: { type: Date, required: false },
    device_batch: { type: String, required: false },
    residence_lga: { type: String, required: false },
    residence_state: { type: String, required: false },
  },
  approver: { type: Schema.Types.ObjectId, required: false },
  approval_date: { type: Date, required: false },
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('edit', EditSchema);
// module.exports = EditSchema;
