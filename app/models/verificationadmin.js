// grab the packages that we need for the admin model
const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
mongoose.Promise = require('bluebird');
const {nbuildConnection} = require('../../config/dbNbuild')

const { Schema } = mongoose.Schema;

// verificationadmin schema
const VerificationAdminSchema = new Schema({
  username: { type: String, required: true, index: { unique: true } },
  password: { type: String, select: false },
  state: { type: String, required: true },
  date_created: { type: Date, default: Date.now },
  last_login: Date,
  disabled: { type: Boolean, default: false },
});

// hash the password before the admin is saved
VerificationAdminSchema.pre('save', (next) => {
  const admin = this;

  // hash the password only if the password has been changed or admin is new
  if (!admin.isModified('password')) return next();

  // generate the hash
  return bcrypt.hash(admin.password, null, null, (err, hash) => {
    if (err) return next(err);

    // change the password to the hashed version
    admin.password = hash;
    return next();
  });
});

// method to compare a given password with the database hash
VerificationAdminSchema.methods.comparePassword = (password) => {
  const admin = this;
  return bcrypt.compareSync(password, admin.password);
};

// return the model
module.exports = nbuildConnection.model('verificationadmin', VerificationAdminSchema);
