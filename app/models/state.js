const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const {nbuildConnection} = require('../../config/dbNbuild')

const { Schema } = mongoose;

const StateSchema = new Schema({
  state: { type: String, required: true },
  state_id: { type: String, required: true },
  devices_info: [
    {
      device_batch: { type: String, required: false },
      shipped_devices: { type: Number, required: false },
      notified_volunteers: { type: Number, required: false },
    },
  ],
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('state', StateSchema);
