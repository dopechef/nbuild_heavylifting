const mongoose = require('mongoose');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const PayrollSchema = new Schema({
  tag: { type: String, required: true, unique: true },
  type: { type: String, required: true, enum: ['master', 'supplementary'] },
  status: {
    type: String,
    required: true,
    enum: ['unauthorized', 'generated', 'vetted', 'approved'],
  },
  date: { type: Date, required: true },
  total_amount_paid: { type: Number, required: false },
  total_deductions: { type: Number, required: false },
  volunteers_paid: { type: Number, required: false }, // count of volunteers paid
  volunteers_unpaid: { type: Number, required: false }, // count of volunteers unpaid
  auth_token_generate: { type: String, required: true },
  auth_token_vetted: { type: String, required: false },
  auth_token_approve: { type: String, required: false },
  generated: {
    by: { type: Schema.Types.ObjectId, required: true, ref: 'Admin' },
    date: { type: Date, required: true, default: Date.now },
    complete: { type: Boolean, default: false, required: true },
  },
  vetted: {
    by: { type: Schema.Types.ObjectId, required: false, ref: 'Admin' },
    date: { type: Date },
  },
  approved: {
    by: { type: Schema.Types.ObjectId, required: false, ref: 'Admin' },
    date: { type: Date },
  },
  statistics: {
    paid: {
      gender: {
        male: { type: Number, required: false },
        female: { type: Number, required: false },
      },
    },
    unpaid: {
      gender: {
        male: { type: Number, required: false },
        female: { type: Number, required: false },
      },
      status: {
        resigned: { type: Number, required: false },
        deceased: { type: Number, required: false },
        hold: { type: Number, required: false },
        exited: { type: Number, required: false },
      },
    },
  },
});

// module.exports = PayrollSchema;
module.exports = nbuildConnection.model('payroll', PayrollSchema);
