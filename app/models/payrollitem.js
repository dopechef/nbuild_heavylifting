const mongoose = require('mongoose');

const { Schema } = mongoose;
const {nbuildConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');

const PayrollItemSchema = mongoose.Schema({
  volunteer_info: {
    firstname: { type: String, required: false },
    middlename: { type: String, required: false },
    surname: { type: String, required: false },
    gender: { type: String, required: false },
    state: { type: String, required: false },
    center_id: { type: Schema.Types.ObjectId, required: false },
    phone: { type: String, required: false },
    email: { type: String, required: false },
    nuban: { type: String, required: false },
    bank_name: { type: String, required: false },
    bvn: { type: String, required: false },
  },
  payroll_id: { type: Schema.Types.ObjectId, required: true, ref: 'Payroll' },
  volunteer: { type: Schema.Types.ObjectId, required: true, ref: 'Volunteer' },
  salary: { type: Number, required: false },
  deduction: { type: Number, required: false },
  amount_paid: { type: Number, required: false },
  status: {
    type: String,
    enum: ['paid', 'hold', 'deceased', 'resigned', 'enrolled', 'payroll_exempt', 'exited'],
  },
});

// module.exports = PayrollItemSchema;
module.exports = nbuildConnection.model('payrollitem', PayrollItemSchema);
