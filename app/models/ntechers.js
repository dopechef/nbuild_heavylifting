const mongoose = require('mongoose');
const { Schema } = mongoose;
const {ntechConnection} = require('../../config/dbNbuild')

mongoose.Promise = require('bluebird');
const {PROGRAM_TYPE, TRAINING_STATUS, STATUS} = require('../../util/constants.helper');

const NtechersSchema = new Schema({
  sn: { type: String, required: false },
  fname: { type: String, required: false },
  mname: { type: String, required: false },
  lname: { type: String, required: false },
  address: { type: String, required: false },
  gender: { type: String, required: false, enum: ['M', 'F'] },
  dob: { type: Date, required: false },
  phone: { type: String, required: false, index: true, unique:true },
  email: { type: String, required: false, unique:true },
  bvn: { type: String, required: false, index: true, unique:true },
  origin_state: { type: String, required: false },
  origin_lga: { type: String, required: false },
  residence_state: { type: String, required: false },
  residence_lga: { type: String, required: false },
  bank_name: { type: String, required: false },
  nuban: { type: String, 
    required: false, 
    unique:true 
  },
  created_at: { type: String, required: false },
  program: { 
            type: String, 
            required: false , 
            enum:[PROGRAM_TYPE.SOFTWARE, PROGRAM_TYPE.HARDWARE], 
            index:true,
          },//1 = software, 2 = hardware
  centre_id: { type: Schema.Types.ObjectId, required: false },
  //ref_num: { type: String, required: true },
  batch: { type: Number, required: true, default:1 },
  score: { type: Number, required: true },
  spread: { type: String, required: true, },
  //profile_pic: { type: Schema.Types.ObjectId, required: false },
  geo_zone:{type:String, required:true},
  id_card: {type:String},
  profile_picture:{type:String},
  training_status:{
    type:String, 
    enum:[
      TRAINING_STATUS.STILL_IN_TRAINING, 
      TRAINING_STATUS.GRADUATED, 
      TRAINING_STATUS.NOT_GRADUATE
    ],
    default:TRAINING_STATUS.STILL_IN_TRAINING
  },
  status: {
    type:String,
    index: true,
    enum:[
      STATUS.HOLD, 
      STATUS.DECEASED, 
      STATUS.RESIGNED, 
      STATUS.ENROLLED 
    ],
    default: STATUS.ENROLLED
  },
  nibss_validated: { type: Boolean, required: true, default: false },
  registered:{type:Boolean, default:true},
  work_placement:{type:String},
  work_placement_letter:{type:String},
  work_placement_address:{type:String},
  special_registration: { type: Boolean, required: false, default: false },
  deployed: { type: Boolean, required: false, default: false },
  nibss_validated_reason: {type: String},
  verified: { type: Boolean, required: true, default: false },
},
{
  toObject:{virtuals:true},
  toJSON:{virtuals:true},
  timestamps:true
});

NtechersSchema.virtual('program_name').get(()=>{
  return this.program != null && this.program === PROGRAM_TYPE.SOFTWARE ? 'Software':'Hardware'
})

// create the model for users and expose it to our app
module.exports = ntechConnection.model('ntechers', NtechersSchema);
