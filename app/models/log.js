const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const {nbuildConnection} = require('../../config/dbNbuild')

const { Schema } = mongoose;

const LogSchema = new Schema({
  event: { type: String, enum: ['view', 'auth', 'create', 'edit', 'payroll'], required: true },
  title: { type: String, required: true, trim: true },
  src_ip: { type: String, required: true },
  description: { type: String, required: true, trim: true },
  admin: { type: Schema.Types.ObjectId, required: true, ref: 'Admin' },
  created_at: { type: Date, required: true, default: Date.now },
  geo: {
    host: { type: String },
    ip: { type: String },
    rdns: { type: String },
    asn: { type: String },
    isp: { type: String },
    country_name: { type: String },
    country_code: { type: String },
    region: { type: String },
    city: { type: String },
    postal_code: { type: String },
    continent_code: { type: String },
    latitude: { type: Number },
    longitude: { type: Number },
    dma_code: { type: Number },
    area_code: { type: Number },
    timezone: { type: String },
    datetime: { type: String },
  },
});

// create the model for users and expose it to our app
module.exports = nbuildConnection.model('log', LogSchema);
// module.exports = LogSchema;
