// grab the packages that we need for the admin model
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const {nbuildConnection, ntechConnection} = require('../../config/dbNbuild')
const { Schema } = mongoose;

// center admin schema
const CentreAttendanceSchema = new Schema({
  name: { type: String, required: true },
  days: { type: Number, required: true },
  volunteer_count: { type: Number, required: true },
  centre_id: { type: Schema.Types.ObjectId, required: true },
  month: { type: String, required: true },
  year: { type: String, required: true },
  date_created: { type: Date, default: Date.now },
  status: { type: String, default: 'Generated', enum: ['Processing', 'Generated', 'Closed'] },
});

module.exports = {
  ntech: ntechConnection.model('centreattendance', CentreAttendanceSchema),
  nbuild: nbuildConnection.model('centreattendance', CentreAttendanceSchema)
}
