exports.volunteer = require('./routes/volunteerRoute');
exports.payrolls = require('./routes/payrollsRoute');
exports.payments = require('./routes/paymentsRoute');
exports.centres = require('./routes/centresRoute');
exports.attendance = require('./routes/attendanceRoute');
exports.exportDocs = require('./routes/exportRoute');