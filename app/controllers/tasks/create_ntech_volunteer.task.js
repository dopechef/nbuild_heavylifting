const Excel = require('exceljs');
//const createVolunteer = require('../volunteers/createNtech');
//const redis = require('redis');
const VolunteerModel = require('../../models/ntechers');
const Guard = require('../../controllers/volunteers/guard.helper');

const createVolunteer = (volunteer) => {
    return new Promise(async (resolve, reject)=>{
        try {
            const existsInOtherProgram = await Guard.validate_user_exists(volunteer)
            if(!existsInOtherProgram.data.success){
                throw new Error(existsInOtherProgram.data.message)
            }
            const checkVolunteerExist = await VolunteerModel.findOne(
                {
                  $or: [
                    { bvn: volunteer.bvn },
                    { nuban: volunteer.nuban },
                    { phone: volunteer.phone },
                    { email: volunteer.email },
                  ],
                })
            if(checkVolunteerExist == null){
                console.log('starting save')
                const saveVol = await VolunteerModel.insertMany(volunteer)
                console.log('saved')
                return resolve('user saved')
            }else{
                return resolve('User Exists')
            }
        }catch(e){
            console.log()
            return resolve(e)
            /* return res.json({
                success: false,
                message: 'Error creating volunteer',
                error: e,
            }); */
        }
    })
};

process.on('message', (filePath) => {
    const workbook = new Excel.Workbook();
    workbook.xlsx.readFile(Buffer.from(filePath)).then(async () => {
        const volPromises = []
        workbook.eachSheet((worksheet) => {
            worksheet.eachRow((row, rowNumber) => {
                if (rowNumber !== 1) {
                    const newVol = {
                        address:row.values[1],
                        batch: row.values[2],
                        dob: new Date(row.values[3]) || null,
                        bvn:  row.values[4],
                        centre_id:  row.values[5],
                        email:  row.values[6] || '',
                        fname:  row.values[7]||'',
                        gender:  row.values[8],
                        geo_zone:  row.values[9],
                        lname:  row.values[10]||'',
                        mname:  row.values[11] == null ? '':row.values[11],
                        nibss_validated:  false,
                        origin_lga:  row.values[13] || '',
                        origin_state:  row.values[14] || '',
                        phone:  row.values[15],
                        program:  ""+row.values[16],
                        registered: false,
                        residence_lga:  row.values[18],
                        residence_state:  row.values[19],
                        spread:  'SOUTH',
                        status:  row.values[22],
                        training_status:  row.values[23],
                        score: 0,
                      }
                      volPromises.push(createVolunteer(newVol))
                }
            })
        })
        const processData = await Promise.all(volPromises)
        console.log(processData)
        process.send('done');
    })
});

