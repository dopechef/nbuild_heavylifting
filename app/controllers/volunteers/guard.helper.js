const {ObjectId} = require('mongoose').Types; 
const NtechersModel = require('../../models/ntechers');
const axios = require('axios');

class Guard {
    async slot_gurard (center) {
        const capacity = center.capacity
        const candidatesCount = await NtechersModel.count({_id:center._id})
        //console.log(capacity, candidatesCount)
        if(capacity === 0 || candidatesCount >= capacity){
            return Promise.resolve(false)
        }
        return Promise.resolve(true)
    }

    async is_valid_location (center, {residence_state, geo_zone, program}) {
        //console.log(center.geozone, geo_zone, center.program, program)
        return Promise.resolve(center.geozone === geo_zone  && center.program === program)
    }

    validate_user_exists (data) {
        return axios.post(
                process.env.FEDERATION_SERVER, 
                data
            )
    }
}

module.exports = new Guard();