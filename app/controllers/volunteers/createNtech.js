const VolunteerModel = require('../../models/ntechers');
const Guard = require('./guard.helper');

const createVolunteer = async (volunteer) => {
    //return new Promise(async (resolve, reject)=>{
        try {
            const existsInOtherProgram = await Guard.validate_user_exists(volunteer)
            if(!existsInOtherProgram.data.success){
                throw new Error(existsInOtherProgram.data.message)
            }

            const existsQuery = []
            const matchQuery = []

            if(volunteer.bvn != null){
                existsQuery.push({bvn:{$exists:true}})
                matchQuery.push({ bvn: volunteer.bvn })
            }

            if(volunteer.nuban){
                existsQuery.push({nuban:{$exists:true}})
                matchQuery.push({ nuban: volunteer.nuban })
            }

            if(volunteer.phone){
                existsQuery.push({phone:{$exists:true}})
                matchQuery.push({ phone: volunteer.phone })
            }

            if(volunteer.email){
                existsQuery.push({email:{$exists:true}})
                matchQuery.push({ email: volunteer.email })
            }

            const checkVolunteerExist = await VolunteerModel.findOne(
                {
                  $and:[
                    {
                        $and: existsQuery
                    },
                    {
                        $or: matchQuery
                    }
                  ]
            });
            if(checkVolunteerExist) {
                throw new Error('User exists');
            }
            const saveVol = await VolunteerModel.insertMany(volunteer)
            console.log('saved', saveVol._id)
            /* if(checkVolunteerExist == null){
                console.log('starting save')
                
                return resolve('user saved')
            }else{
                return resolve('User Exists')
            } */
        }catch(e){
            //console.log(volunteer.bvn, volunteer.phone, volunteer.email, volunteer.nuban, volunteer.fname, volunteer.lname,  volunteer.mname)
            //console.log(matchQuery)
            console.log(e)
            /* return res.json({
                success: false,
                message: 'Error creating volunteer',
                error: e,
            }); */
        }
   // })
};

module.exports = createVolunteer;
