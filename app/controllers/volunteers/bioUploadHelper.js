const cloudinaryHelper = require('../../../util/cloudinary.helper');

module.exports = async (req) => {
    try{
        const {id_card, profile_picture, work_placement_letter} = req.files
        
        const promises = []
        if(id_card) {
            promises.push(cloudinaryHelper(id_card[0].path))
        }
        if(profile_picture){
            promises.push(cloudinaryHelper(profile_picture[0].path))
        }
        if(work_placement_letter){
            promises.push(cloudinaryHelper(work_placement_letter[0].path))
        }

        const imgRes = await Promise.all(promises)
        
        const data = {}
        if(work_placement_letter){
            data['work_placement_letter'] = imgRes[0].secure_url
        }else{
            const setProfilePicture = (file) => {
                data['profile_picture'] = file
            }
            const setIdCardPicture = (file) => {
                data['id_card'] = file
            }
            if(id_card && profile_picture){
                setIdCardPicture(imgRes[0].secure_url)
                setProfilePicture(imgRes[1].secure_url)
            }else if(id_card && !profile_picture){
                setIdCardPicture(imgRes[0].secure_url)
            }else if(!id_card && profile_picture){
                setProfilePicture(imgRes[0].secure_url)
            }
        }
        return Promise.resolve(data)
    }catch(e){
        console.log(e)
        return Promise.reject('Error occurred while uploading!')
    }
}