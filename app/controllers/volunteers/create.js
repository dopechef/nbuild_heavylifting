const Joi = require('joi');
const Volunteer = require('../../models/volunteer')
const NbuildCenter = require('../../models/nbuildcentre');
const bioUploadHelper = require('./bioUploadHelper');
const Guard = require('./guard.helper');
//const ResponseHelper = require('../../../utils/responseHelper');
const {TRADES_ARRAY} = require('./constants')

const createVolunteer = (req, res) => {
  console.log(req.body)
    const volunteerSchema = Joi.object().keys({
      fname: Joi.string()
        .alphanum()
        .required(),
      mname: Joi.string()
                .alphanum(),
      lname: Joi.string()
        .alphanum()
        .required(),
      address: Joi.string().required(),
      state: Joi.string()
        .uppercase()
        .required(),
      lga: Joi.string().required(),
      gender: Joi.string()
        .only(['M', 'F'])
        .required(),
      birth_day: Joi.number()
        .integer()
        .min(1)
        .max(31)
        .required(),
      birth_month: Joi.number()
        .integer()
        .min(1)
        .max(12)
        .required(),
      birth_year: Joi.number()
        .integer()
        .min(1800)
        .max(2013)
        .required(),
      phone: Joi.string().required(),
      email: Joi.string().email(),
      bvn: Joi.number()
              .integer()
              //.min(11)
              //.max(11)
              .required(),
      bank_name: Joi.string().required(),
      nuban: Joi.string().required(),
      trade_allocation: Joi.string()
        .only(TRADES_ARRAY)
        .uppercase()
        .required(),
      batch: Joi.string().required(),
      centre_id: Joi.string().required(),
      //work_placement: Joi.string(),
      id_card: Joi.any(),
      profile_picture: Joi.any(),
      ward: Joi.string(),
      origin_state: Joi.string(),
      origin_lga: Joi.string(),
      spread:Joi.string(),
      
    });
  
    Joi.validate(req.body, volunteerSchema, (err) => {
      if (err) {
        console.log(err);
        return res.json({
          success: false,
          message: 'Check your request params',
          err,
        });
      }
      Volunteer.findOne(
        {
          $or: [
            { bvn: req.body.bvn },
            { nuban: req.body.nuban },
            { phone: req.body.phone },
            { email: req.body.email },
          ],
        },
        (e, d) => {
          if (e) {
            return res.json({
              success: false,
              message: 'Error creating volunteer',
              error: e,
            });
          }
          // eslint-disable-next-line
          if (d != null) {
            return res.json({
              success: false,
              message: 'Error !, a volunteer has already been created with these details',
            });
          }
          return NbuildCenter.findOne({ _id: req.body.centre_id }, async (errr, centre) => {
            if (errr) {
              //console.log(errr);
              return res.json({
                success: false,
                message: 'No centre found for centre_id passed',
                errr,
              });
            }
            if(centre.status === 'inactive'){
                return res.json({
                    success:false,
                    message:`Registration is closed for this center!`
                })
            }
            
            try{
                const existsInOtherProgram = await Guard.validate_user_exists(req.body)
                //console.log(existsInOtherProgram)
                if(!existsInOtherProgram.data.success){
                    throw new Error(existsInOtherProgram.data.message)
                }
                // perform upload
                const isSlotAvailable = await Guard.slot_gurard(centre, req.body)
                if(!isSlotAvailable){
                    return res.json({
                        success:false,
                        message:`There are no more slots available for this trade in this center`
                    })
                }

                const uploadRes = await bioUploadHelper(req)
                Object.assign(req.body, uploadRes)

                const saveVolunteer = (registered = true, deployed = true, special_registration = true) => {
                    if(req.body.batch === '3'){
                        registered = false
                        special_registration = false
                    }
                    req.body.centre_allocated = true;
                    req.body.centre_allocation = centre.CENTRE;
                    req.body.status = 'enrolled';
                    req.body.training_status = 'Still in training';
                    req.body.verified = true;
                    req.body.deployed = deployed;
                    req.body.registered = registered;
                    req.body.special_registration = special_registration;

                    Volunteer.insertMany(req.body, (error, doc) => {
                        if (error) {
                            console.log(error);
                            return res.json({
                                success: false,
                                message: 'Cannot create Volunteer',
                                error,
                            });
                        }
                        return res.json({
                            success: true,
                            message: 'Volunteer registered Successfully',
                            data: doc,
                            });
                    });
                }
                
                switch (req.body.trade_allocation) {
                    case 'AUTOMOBILE':
                        saveVolunteer()
                            /* req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            }); */
                        break;
                    case 'CARPENTRY & JOINERY':
                        saveVolunteer()
                           /*  req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            console.log('body', req.body);
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            });
                            */
                        break;
                    case 'ELECTRICAL':
                        saveVolunteer()
                       /*      req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            console.log('body', req.body);
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            }); */
                        break;
                    case 'MASONRY':
                        saveVolunteer()
                           /*  req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            console.log('body', req.body);
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            });
                         */
                        break;
                    case 'PAINTING & DECORATING':
                        saveVolunteer()
/*                             req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            console.log('body', req.body);
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            });
                     */      
                        break;
                    case 'PLUMBING':
                        saveVolunteer()
                        /* 
                            req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            });
                          */
                        break;
                    case 'WELDING':
                        saveVolunteer()
                           /*  req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            }); */
                        break;
                    case 'AGRIC_TECHNOLOGY':
                        saveVolunteer()
                          /*   req.body.centre_allocated = true;
                            req.body.centre_allocation = centre.CENTRE;
                            req.body.status = 'enrolled';
                            req.body.training_status = 'Still in training';
                            req.body.verified = true;
                            req.body.deployed = true;
                            req.body.registered = true;
                            req.body.special_registration = true;
                            Volunteer.insertMany(req.body, (error, doc) => {
                                if (error) {
                                console.log(error);
                                return res.json({
                                    success: false,
                                    message: 'Cannot create Volunteer',
                                    error,
                                });
                                }
                                return res.json({
                                success: true,
                                message: 'Volunteer registered Successfully',
                                data: doc,
                                });
                            }); */
                          
                        break;
                    case 'AGRIC TECHNOLOGY':
                        saveVolunteer();
                        break;
                    case 'HOSPITALITY':
                        saveVolunteer();
                        break;
                    default:
                        return res.json({
                        success: false,
                        message: 'Input the right trade allocation',
                        });
                    }
            }catch(e){
                const message = e.message ? e.message:'An error occurred!'
                res.json({
                  success:false,
                  message,
                  data:e
                })
                //res.send(ResponseHelper.errorResponse())
            }
            return null;
          });
        },
      );
      return null;
    });
};

module.exports = createVolunteer