const Volunteer = require('../../models/ntechers');
const Excel = require('exceljs');

const ntechersExport = (req, res) => {
    const query = {registered:true};
    let selectArgs = '';
    let mongooseQuery;
    
    //fields to include 

    if (req.query.filter !== undefined) {
      // This allows us specify what volunteers to return e.g gender=male,
      const tokens = req.query.filter.split('|');
      let values;
      //console.log(req.query, tokens)
      for (let i = 0; i < tokens.length; i += 1) {
        if (req.query[tokens[i]]) {
        
          values = req.query[tokens[i]].split(',');
          if(tokens[i] === 'geo_zone'){
            values = values.map(v=>v.toUpperCase())
            //query[tokens[i]] = { $in: values };
            //continue
          }

          if(tokens[i] === 'state'){
            values = values.map(v=>v.toUpperCase())
            //query[tokens[i]] = { $in: values };
            //continue
          }
          
          if(tokens[i] === 'lga'){
            values = values.map(v=>v.toLowerCase())
            //query[tokens[i]] = { $in: values };
            //continue
          }

          //if(!Volunteer.schema.path(tokens[i])){
            //  continue
          //}
          
          //check the field type and convert 
          if(Volunteer.schema.path(tokens[i]) != null && Volunteer.schema.path(tokens[i]).instance === 'Number'){
            values = values.map(v=>parseInt(v))
            //query[tokens[i]] = { $in: values };
            //continue
          }
          
          query[tokens[i]] = { $in: values };
          // query[tokens[i]] = req.query[tokens[i]];
        }
      }
    }
    

    const format = req.query.format
    if (format === 'excel2') {
      // replace '|' with spaces ' '
      selectArgs = req.query.fields.replace(/\|/g, ' ');
    }
    //console.log(query, 'ntech')
    if (format === 'excel2') {
        mongooseQuery = Volunteer.find(query).select(selectArgs);
    } else {
        mongooseQuery = Volunteer.find(query);
    }
    
    mongooseQuery.exec((err, volunteers) => {
      if (err) {
        //console.log(err);
        return res.json({ success: false, message: 'Error retrieving hold volunteers' });
      }
      
      if (format === 'excel') {
        const workbook = new Excel.Workbook();
  
        workbook.creator = 'NTECH Admin Portal';
        workbook.lastModifiedBy = 'NTECH Admin Portal';
        workbook.created = new Date();
        workbook.modified = new Date();
  
        const sheetVolunteers = workbook.addWorksheet('Ntech_Volunteers');
  
        sheetVolunteers.columns = [
          {
            header: 'S/N', 
            key: 'id', 
            width: 10, 
            style: { 
                alignment: { vertical: 'top', horizontal: 'right' } 
            },
          },
          { header: 'NTECH ID', key: '_id', width: 32 },
          { header: 'First Name', key: 'fname', width: 32 },
          { header: 'Last Name', key: 'lname', width: 32 },
          { header: 'Middle Name', key: 'mname', width: 32 },
          { header: 'Gender', key: 'gender', width: 16 },
          { header: 'Phone', key: 'phone', width: 32 },
          { header: 'Email', key: 'email', width: 32 },
          { header: 'Program', key: 'program', width: 32 },
          { header: 'Status', key: 'status', width: 32 },
          { header: 'Bank BVN', key: 'bvn', width: 32 },
          { header: 'Bank Name', key: 'bank_name', width: 32 },
          { header: 'Nuban', key: 'nuban', width: 32 },
          { header: 'Account Name', key: 'accountname', width: 32 },
          { header: 'Residence State', key: 'residence_state', width: 32 },
          { header: 'Residence LGA', key: 'residence_lga', width: 32 },
          { header: 'Date Of Birth', key:'birthday', width:32},
          //{ header: 'Centre', key:'centre_allocation', width:32}, 
          { header: 'Nibss Validated', key:'nibss_validated', width:32}, 
          { header: 'Geozone', key:'geozone', width:32}, 
          { header: 'Training Status', key:'training_status', width:32}, 
        ];
  
        sheetVolunteers.getRow(1).font = { size: 14, bold: true };
  
        sheetVolunteers.getRow(1).fill = {
          type: 'pattern',
          pattern: 'mediumGray',
          fgColor: { argb: 'FFFFFF00' },
          bgColor: { argb: 'FF0000FF' },
        };
  
        for (let i = 0; i < volunteers.length; i += 1) {
          const {
                fname, lname, mname, gender, phone, email, status, bvn, bank_name,
                nuban, residence_state, residence_lga, birthday, program, 
                centre_allocation, nibss_validated, geozone, training_status, batch
            } = volunteers[i]
          
          sheetVolunteers.addRow({
            id: (i + 1),
            unique_id: volunteers[i]._id,
            fname,
            lname,
            mname,
            gender,
            phone,
            email,
            //program: volunteers[i].program,
            status,
            bank_name,
            bankbvn: bvn,
            residence_state,
            residence_lga,
            birthday,
            nuban,
            program,
            centre_allocation,
            nibss_validated,
            geozone,
            training_status,
            batch
          });
        }
  
        res.set('Content-disposition', 'attachment; filename=volunteers.xlsx');
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        workbook.xlsx.write(res);
      } else if (req.query.format === 'excel2') {
        const workbook = new Excel.Workbook();
  
        workbook.creator = 'NTECH Admin Portal';
        workbook.lastModifiedBy = 'NTECH Admin Portal';
        workbook.created = new Date();
        workbook.modified = new Date();
  
        const sheetVolunteers = workbook.addWorksheet('Ntech_Volunteers');
  
        const sheetColumns = [
          {
            header: 'S/N', key: 'id', width: 10, 
            style: { alignment: { vertical: 'top', horizontal: 'right' } },
          },
        ];
  
        const fields = req.query.fields.split('|');
        //console.log(fields)
        for (let i = 0; i < fields.length; i += 1) {
          if (fields[i] === '_id') {
            sheetColumns.push({ header: 'NPVN ID', key: 'unique_id', width: 32 });
          } else if (fields[i] === 'fname') {
            sheetColumns.push({ header: 'First Name', key: 'fname', width: 32 });
          } else if (fields[i] === 'lname') {
            sheetColumns.push({ header: 'Last Name', key: 'lname', width: 32 });
          } else if (fields[i] === 'mname') {
            sheetColumns.push({ header: 'Middle Name', key: 'mname', width: 32 });
          } else if (fields[i] === 'gender') {
            sheetColumns.push({ header: 'Gender', key: 'gender', width: 32 });
          } else if (fields[i] === 'dob') {
            sheetColumns.push({ header: 'Date of Birth', key: 'birthday', width: 32 });
          } else if (fields[i] === 'phone') {
            sheetColumns.push({ header: 'Phone', key: 'phone', width: 32 });
          } else if (fields[i] === 'email') {
            sheetColumns.push({ header: 'Email', key: 'email', width: 32 });
          } else if (fields[i] === 'nibss_validated') {
            sheetColumns.push({ header: 'Nibss Validated', key: 'nibss_validated', width: 32 });
          } else if (fields[i] === 'status') {
            sheetColumns.push({ header: 'Status', key: 'status', width: 32 });
          } else if (fields[i] === 'bvn') {
            sheetColumns.push({ header: 'Bank BVN', key: 'bvn', width: 32 });
          } else if (fields[i] === 'nuban') {
            sheetColumns.push({ header: 'Nuban', key: 'nuban', width: 32 });
          } else if (fields[i] === 'bank_name') {
            sheetColumns.push({ header: 'Bank Name', key: 'bank_name', width: 32 });
          } else if (fields[i] === 'centre_allocation') {
            sheetColumns.push({ header: 'Centre', key: 'centre_allocation', width: 32 });
          } else if (fields[i] === 'residence_state') {
            sheetColumns.push({ header: 'State', key: 'residence_state', width: 32 });
          } else if (fields[i] === 'residence_lga') {
            sheetColumns.push({ header: 'LGA', key: 'residence_lga', width: 32 });
          } else if (fields[i] === 'batch') {
            sheetColumns.push({ header: 'Batch', key: 'batch', width: 32 });
          } else if (fields[i] === 'training_status') {
            sheetColumns.push({ header: 'Training Status', key: 'training_status', width: 32 });
          } else if (fields[i] === 'program') {
            sheetColumns.push({ header: 'Program', key: 'program', width: 32 });
          }
        }
  
        sheetVolunteers.columns = sheetColumns;
  
        sheetVolunteers.getRow(1).font = { size: 14, bold: true };
  
        sheetVolunteers.getRow(1).fill = {
          type: 'pattern',
          pattern: 'mediumGray',
          fgColor: { argb: 'FFFFFF00' },
          bgColor: { argb: 'FF0000FF' },
        };
  
        for (let j = 0; j < volunteers.length; j += 1) {
          const volunteerObject = {};
          volunteerObject.id = (j + 1);
          
          for (let i = 0; i < fields.length; i += 1) {
            if (fields[i] === '_id') {
              volunteerObject._id = volunteers[j]._id;
            } else if (fields[i] === 'fname') {
              volunteerObject.fname = volunteers[j].fname;
            } else if (fields[i] === 'lname') {
              volunteerObject.lname = volunteers[j].lname;
            } else if (fields[i] === 'mname') {
              volunteerObject.mname = volunteers[j].mname;
            } else if (fields[i] === 'gender') {
              volunteerObject.gender = volunteers[j].gender;
            } else if (fields[i] === 'phone') {
              volunteerObject.phone = volunteers[j].phone;
            } else if (fields[i] === 'email') {
              volunteerObject.email = volunteers[j].email;
            } else if (fields[i] === 'program') {
              volunteerObject.program = volunteers[j].program_name;
            } else if (fields[i] === 'status') {
              volunteerObject.status = volunteers[j].status;
            } else if (fields[i] === 'bvn') {
              volunteerObject.bvn = volunteers[j].bvn;
            } else if (fields[i] === 'bank_name') {
              volunteerObject.bank_name = volunteers[j].bank_name;
            } else if (fields[i] === 'batch') {
              volunteerObject.batch = volunteers[j].batch;
            } else if (fields[i] === 'nuban') {
              volunteerObject.nuban = volunteers[j].nuban;
            } else if (fields[i] === 'residence_state') {
              volunteerObject.residence_state = volunteers[j].residence_state;
            } else if (fields[i] === 'residence_lga') {
              volunteerObject.residence_lga = volunteers[j].residence_lga;
            }else if(fields[i] === 'nibss_validated'){
              volunteerObject.nibss_validated = volunteers[j].nibss_validated === true ? 'Yes':'No'
            }else if(fields[i] === 'training_status'){
                volunteerObject.training_status = volunteers[j].training_status
            }
          }
          sheetVolunteers.addRow(volunteerObject);
        }
  
        res.set('Content-disposition', 'attachment; filename=netch_volunteers.xlsx');
        res.set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        workbook.xlsx.write(res);
      } else {
        res.json({ success: true, data: volunteers });
      }
    });
  };

  

  module.exports = ntechersExport