const sms = require('../../util/sms.js');
const email = require('../../util/email.js');
const Volunteer = require('../models/volunteer');
const NtechVolunteer = require('../models/ntechers');
const {nbuild, ntech} = require('../models/volunteerattendance');
const CentreAttendance = require('../models/centreattendance');

const generateAttendance = async (req, res) => {
  // eslint-disable-next-line
  const { attendance_id, centre_id, admin, date_marked } = req.body;
  try {
    const promises = await fetchVolunteers(centre_id, date_marked, attendance_id);
    const vols = await Promise.all(promises);
    const centerAttendance = await CentreAttendance.nbuild.updateOne(
      { _id: attendance_id },
      { $set: { status: 'Generated' }
    })
    
    const date = new Date(date_marked);
    sendEmail(admin.email, date, promises.length)

    return res.json({success:true, centerAttendance});
  } catch (error) {
    console.log(error)
    return res.json({
      success: false,
      error,
    });
  }
};

const generateNtechAttendance = async (req, res) => {
  const { attendance_id, centre_id, admin, date_marked } = req.body;
  console.log(attendance_id, 'ntech')
  try {
    const promises = await fetchVolunteers(centre_id, date_marked, attendance_id, false);
    const vols = await Promise.all(promises);
    console.log(vols)
    const centerAttendance = await CentreAttendance.ntech.updateOne(
      { _id: attendance_id },
      { $set: { status: 'Generated' }
    })
    
    const date = new Date(date_marked);
    const adminEmail = admin == null ? null : admin.email
    sendEmail(adminEmail, date, promises.length, false)

    return res.json({success:true, centerAttendance});
  } catch (error) {
    console.log(error)
    return res.json({
      success: false,
      error,
    });
  }
}

const fetchVolunteers = (centre_id,date_marked, attendance_id, isNbuild = true) => {
    const query = { centre_id, registered:true }
    
    return new Promise(async (resolve, reject)=>{
        try{
          const volArray = isNbuild ? await Volunteer.find(query) : await NtechVolunteer.find(query);
          
          const promises = []
          volArray.forEach((element) => {
              const {
                // eslint-disable-next-line
                _id,
                fname,
                mname,
                lname,
                centre_id,
                phone,
                batch,
              } = element;
            
              const data = {
                volunteer_id: _id,
                fname,
                mname,
                lname,
                phone,
                centre_id,
                attendance_id,
                percentage: 0,
                date_marked,
                batch
              }

              if(isNbuild){
                data.trade_allocation = element.trade_allocation
                promises.push(nbuild.insertMany(data))
              }else{
                data.program = element.program
                promises.push(ntech.insertMany(data))
              }
        });
        console.log(promises)
        resolve(promises);
      }catch(e){
        console.log(e)
        reject(e)
      }
    });
}

const sendEmail = (recepient, date, volunteerCount, nbuild = true) => {
  const project = nbuild ? 'Nbuild':'Ntech';
  const locale = "en-us";
  const monthName = date.toLocaleString(locale, { month: "long" });
  const yearName = date.getFullYear().toString();

  //const sendTo = recepient || 'doyin@softcom.ng'
  const sendTo = 'doyin@softcom.ng'
  email.send(
    sendTo,
    `${project} Centre Attendance (${monthName} ${yearName})`,
    `Your Centre attendance generated successfully 
    for the month of ${monthName} ${yearName}:
    Attendance created for ${volunteerCount} volunteers.
    `
  );
};

module.exports = {
  generateAttendance,
  generateNtechAttendance
};
