const Excel = require('exceljs');
const Volunteer = require('../models/volunteer');
const NtechVolunteers = require('../models/ntechers');
const {REGULATORY_BODIES} = require('../../util/enum.utils');
const regBodyHelper = require('./utils/regBodyHelper.utils');
// upload Payment file /v1/nbuild/admin/payments/uploads

const updateVolunteer = async (req, res) => {
  try {
    const workbook = new Excel.Workbook();
    const {app_name} = req.query
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(() => {
      // use workbook
      workbook.eachSheet((worksheet) => {
        worksheet.eachRow((row, rowNumber) => {
          // skip first row
          if (rowNumber !== 1) {
            const AppVolunteerObject = app_name === 'ntech' ? NtechVolunteers : Volunteer;
            AppVolunteerObject.findOne({ _id: row.values[1] }, '_id', (err, volunteer) => {
              // console.log(date);
              console.log(row.values[1]);

              if (err) {
                console.log(err);
                return;
              }
              if (!volunteer) {
                // No volunteer found, Move to next row
                return;
              }
              const field = {};
              // eslint-disable-next-line
              field[`${req.query.field}`] = row.values[2];
              switch (req.query.field) {
                case 'status':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { status: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                case 'training_status':
                  if (row.values[2] === 'Did not graduate') {
                    Volunteer.updateOne(
                      { _id: row.values[1] },
                      {
                        $set: {
                          training_status: row.values[2],
                          status: 'exited',
                        },
                      },
                      (errr) => {
                        if (errr) {
                          return console.log(errr);
                        }
                        return console.log(
                          `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                            row.values[2]
                          }`,
                        );
                      },
                    );
                  } else {
                    Volunteer.updateOne(
                      { _id: row.values[1] },
                      {
                        $set: { training_status: row.values[2] },
                      },
                      (errr) => {
                        if (errr) {
                          return console.log(errr);
                        }
                        return console.log(
                          `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                            row.values[2]
                          }`,
                        );
                      },
                    );
                  }
                  break;
                case 'nibss':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { nibss_validated: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                case 'gender':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { gender: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                case 'batch':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { batch: parseInt(row.values[2], 10) },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                case 'special_reg':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { special_registration: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                case 'nibss_validated_reason':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { nibss_validated_reason: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                case 'registered':
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { registered: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
                /*case 'lga':
                console.log(row.values[1], row.values[2])
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: { lga: row.values[2] },
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;*/
                default:
                  const data = {}
                  data[req.query.field] = row.values[2]
                  Volunteer.updateOne(
                    { _id: row.values[1] },
                    {
                      $set: data,
                    },
                    (errr) => {
                      if (errr) {
                        return console.log(errr);
                      }
                      return console.log(
                        `volunteer of id ${row.values[1]} ${req.query.field} set to ${
                          row.values[2]
                        }`,
                      );
                    },
                  );
                  break;
              }
            });
          }
        });
      });
    });
    return res.json({
      success: true,
      message: 'Process Ongoing, Check Back later....',
    });
  } catch (error) {
    console.log(error);
    return res.json({
      success: false,
      message: 'Error, somethings wrong',
      error,
    });
  }
};

const addRegulatoryBodies = (req, res) => {
  const workbook = new Excel.Workbook();
  workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
    // use workbook
    const data = []
    workbook.eachSheet((worksheet) => {
      worksheet.eachRow((row, rowNumber) => {
        if (rowNumber !== 1) {
          /*const centre_id = row.values[1]
          
          const bodies = []
          if(row.values[4] === 'true'){
            bodies.push(REGULATORY_BODIES.NDDC)
          }
          if(row.values[5] === 'true'){
            bodies.push(REGULATORY_BODIES.CORBON)
          }
          const vol = {updateMany:{filter:{centre_id}, update:{regulatory_body:bodies}}}*/
          data.push(regBodyHelper(row))
        }
      })
    })
    
    try{
      const updateOp = await Volunteer.bulkWrite(data)
      res.json({success:true, data:{
        records:data.length,
        updates: updateOp.modifiedCount
      }})
    }catch(e){
      res.json({success:false, data:e})
    }
  })
}

const batchCreateNtechVolunteers = async (req, res) => {
  /* const { fork } = require('child_process');
  const path = require('path');
  const worker = fork(path.join(__dirname,'tasks','create_ntech_volunteer.task'));
  worker.on('message', (message) => {
    console.log(message);
    worker.kill();
  });
  worker.send(req.file.path);
  res.json({
    success:true,
    message:`records created!`
  }) */
  const createVolunteer = require('./volunteers/createNtech');
  try {
    const workbook = new Excel.Workbook();
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
      workbook.eachSheet((worksheet) => {
        worksheet.eachRow((row, rowNumber) => {
          // skip first row
          if (rowNumber !== 1) {
            
            const newVol = {
              address:row.values[1],
              batch: row.values[2],
              dob: new Date(row.values[3]) || null,
              bvn:  row.values[4],
              centre_id:  row.values[5],
              email:  row.values[6] || '',
              fname:  row.values[7]||'',
              gender:  row.values[8],
              geo_zone:  row.values[9],
              lname:  row.values[10]||'',
              mname:  row.values[11] == null ? '':row.values[11],
              nibss_validated:  false,
              origin_lga:  row.values[13] || '',
              origin_state:  row.values[14] || '',
              phone:  row.values[15],
              program:  ""+row.values[16],
              registered: false,
              residence_lga:  row.values[18],
              residence_state:  row.values[19],
              spread:  'SOUTH',
              status:  row.values[22],
              training_status:  row.values[23],
              score: 0,
            }
            createVolunteer(newVol)
          }
        })
      })
      res.json({
        success:true,
        message:`records created!`
      })
    });
  }catch(e){
    res.json({
      success:false,
      data:e
    })
  } 
}

const confirmVolunteerExists = (req, res) => {
  const guard = require('./volunteers/guard.helper');

  try {
    const workbook = new Excel.Workbook();
    const volPromises = []
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
      // use workbook
      workbook.eachSheet((worksheet) => {
        worksheet.eachRow(async (row, rowNumber) => {
          
          if (rowNumber !== 1) {
            try{
              const queryData = {
                phone: row.values[5],
                email: row.values[6],
                bvn: row.values[8],
                rowNumber
              }
              volPromises.push(guard.validate_user_exists(queryData))
              /*const {data} = await guard.validate_user_exists(queryData)
              if(data.success === false){
                volPromises.push(result.data)
              }*/
            }catch(e){
              console.log(e)
            }
            //volPromises.push()
          }
        })
      })
      try{
        const result = await Promise.all(volPromises)
        console.log(result)
      }catch(e){
        console.log(e, false)
      }
    })
    res.json({
      success:true,
      message:`records validated!`
    })
  }catch(e){
    console.log(e)
  }
}

const updateRegStatus = (req, res) => {
  const NtechVolunteer = require('../models/ntechers')
  try {
    const workbook = new Excel.Workbook();
    const volPromises = []
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
      // use workbook
      workbook.eachSheet((worksheet) => {
        worksheet.eachRow(async (row, rowNumber) => {
          
          if (rowNumber !== 1) {
            console.log(row.values[1])
            volPromises.push(NtechVolunteer.updateMany({
              _id:row.values[1]},
              {nibss_validated:true}
            ))
          }
        })
      })

      try{
        const result = await Promise.all(volPromises)
        res.json({
          success:true,
          data:result
        })
      }catch(e){
        console.log(e)
        res.json({
          success:false,
          data:e
        })
      }
    })
  }catch(e){
    console.log(e)
  }
} 

module.exports = {
  updateVolunteer,
  addRegulatoryBodies,
  batchCreateNtechVolunteers,
  confirmVolunteerExists,
  updateRegStatus
};
