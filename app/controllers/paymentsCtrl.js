const Excel = require('exceljs');
const Volunteer = require('../models/volunteer');
const Payment = require('../models/payment');
const PaymentStateStat = require('../models/paymentstatestat');

// eslint-disable-next-line
const states_arr = [
  'ABIA',
  'ABUJA FCT',
  'ADAMAWA',
  'AKWA IBOM',
  'ANAMBRA',
  'BAUCHI',
  'BAYELSA',
  'BENUE',
  'BORNO',
  'CROSS RIVER',
  'DELTA',
  'EBONYI',
  'EDO',
  'EKITI',
  'ENUGU',
  'GOMBE',
  'IMO',
  'JIGAWA',
  'KADUNA',
  'KANO',
  'KATSINA',
  'KEBBI',
  'KOGI',
  'KWARA',
  'LAGOS',
  'NASARAWA',
  'NIGER',
  'OGUN',
  'ONDO',
  'OSUN',
  'OYO',
  'PLATEAU',
  'RIVERS',
  'SOKOTO',
  'TARABA',
  'YOBE',
  'ZAMFARA',
];

// eslint-disable-next-line
const calculatePaymentsForStateForDate = (state_id, year, month) => {
  console.log(`${year} ${month}`);

  // JS date object calculates January = 0, February = 1
  const m = month;

  // This is done to ensure we have T00:00:00Z at the end
  const date = new Date('2017-06-10T00:00:00Z');
  date.setFullYear(year, m, 10);

  Volunteer.aggregate(
    [
      {
        $match: {
          state_id,
        },
      },
      {
        $group: {
          _id: null,
          volunteer_ids: {
            $addToSet: '$_id',
          },
        },
      },
    ],
    (err, result) => {
      if (err) {
        console.log(err);
        return;
      }

      if (result[0] === undefined) {
        console.log('Nothin returned from DB');
        console.log(result);
        return;
      }

      Payment.aggregate(
        [
          {
            $match: {
              date,
              volunteer_id: { $in: result[0].volunteer_ids },
            },
          },
          {
            $group: {
              _id: null,
              count: { $sum: 1 },
              amount: { $sum: '$amount' },
            },
          },
        ],
        (errr, obj) => {
          if (err) {
            console.log(errr);
            return;
          }

          if (obj[0] === undefined) {
            console.log('no payment info for this month');
            return;
          }

          // eslint-disable-next-line
          paymentStateStat = new PaymentStateStat();
          // eslint-disable-next-line
          paymentStateStat.state_id = state_id;
          // eslint-disable-next-line
          paymentStateStat.state = states_arr[state_id - 1];
          // eslint-disable-next-line
          paymentStateStat.date = date;
          // eslint-disable-next-line
          paymentStateStat.amount = obj[0].amount;
          // eslint-disable-next-line
          paymentStateStat.count = obj[0].count;
          // eslint-disable-next-line
          paymentStateStat.save(errrr => {
            if (err) {
              console.log(errrr);
              return;
            }
            // eslint-disable-next-line
            console.log(`Payment stat for state saved successfully${states_arr[state_id - 1]}`);
          });
        },
      );
    },
  );
};

const calculatePaymentsForAllStatesForDate = (date) => {
  // get last month's year and month
  // getMonth returns 0 for January
  // date = new Date(date);
  const month = date.getMonth();
  const year = date.getFullYear();
  console.log('CALC');
  console.log(`${year} ${month}`);
  // eslint-disable-next-line
  for (let state_id = 1; state_id < 38; ) {
    calculatePaymentsForStateForDate(state_id.toString(), year, month);
    // eslint-disable-next-line
    state_id += 1;
  }

  // set key to let us know we've calculated for this month and year
  // redisClient.set(redisUtil.createRedisKey('PAYMENT%' + year + '-' + month), 1);
};

// eslint-disable-next-line
const uploadPayments = (req, res, volunteerModel) => {
  const date = new Date('2017-06-10T00:00:00Z');
  const d = new Date(req.query.date);
  const month = d.getMonth();
  const year = d.getFullYear();
  date.setFullYear(year, month);
  try {
    const workbook = new Excel.Workbook();
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(() => {
      workbook.worksheets.forEach((worksheet) => {
        worksheet.eachRow((row, rowNumber) => {
          // skip first row
          if (rowNumber !== 1) {
            volunteerModel.findOne({ _id: row.values[1] }, '_id', (err, volunteer) => {
              // console.log(date);
              console.log(row.values[1]);

              if (err) {
                console.log(err);
              }
              if (!volunteer) {
                console.log('No volunteer found')
                // No volunteer found, Move to next row
                return;
              }
              // count += 1;
              const payment = new Payment();
              // eslint-disable-next-line
              payment.volunteer_id = volunteer._id;
              payment.batch = volunteer.batch;
              payment.date = date;
              // eslint-disable-next-line
              payment.type = row.values[3];
              
              // eslint-disable-next-line
              payment.status = row.values[4];
              try {
                payment.amount = Number(row.values[2]);
              } catch (errr) {
                // Invalid amount found
                console.log(errr);
              }

              payment.save((payError) => {
                if (payError) {
                  console.log(payError);
                  return;
                }
                //console.log('payment updated successfully', '5a66379d9f0129296cc83870');
              });
            });
          }
        });
      });
    });
    res.json({
      success: true,
      message: 'Process Ongoing, Check back later....',
    });
  } catch (error) {
    console.log(error);
    res.json({
      success: false,
      error,
    });
  }
  return calculatePaymentsForAllStatesForDate(d);
};

module.exports = {
  calculatePaymentsForAllStatesForDate,
  calculatePaymentsForStateForDate,
  uploadPayments
};
