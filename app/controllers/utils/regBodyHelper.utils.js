const {REGULATORY_BODIES} = require('../../../util/enum.utils');

/**
 * 
 * @param {*} row Each row of the excel sheet
 * @param {*} isCenterOperation if this is a centre operation then the filter should be for _id and not centre_id
 */
module.exports = (row, isCenterOperation = false) => {
    const centre_id = row.values[1]
          
    const bodies = []
    if(row.values[3] === 'true'){
        bodies.push(REGULATORY_BODIES.NDDC)
    }
    if(row.values[4] === 'true'){
        bodies.push(REGULATORY_BODIES.CORBON)
    }
    if(row.values[5] === 'true'){
        bodies.push(REGULATORY_BODIES.ITP)
    }
    const filter = isCenterOperation ? {_id:centre_id}:{centre_id}
    return {filter, bodies}
    //{updateMany:{filter, update:{regulatory_body:bodies}}}
}