const mongoose = require('mongoose');
const sms = require('../../util/sms.js');
const Email = require('../../util/email.js');
const Volunteer = require('../models/volunteer');
const Payroll = require('../models/payroll');
const PayrollItem = require('../models/payrollitem');
const PayrollStateStat = require('../models/payrollstatestat');

// eslint-disable-next-line
const states_arr = [
  'ABIA',
  'ABUJA FCT',
  'ADAMAWA',
  'AKWA IBOM',
  'ANAMBRA',
  'BAUCHI',
  'BAYELSA',
  'BENUE',
  'BORNO',
  'CROSS RIVER',
  'DELTA',
  'EBONYI',
  'EDO',
  'EKITI',
  'ENUGU',
  'GOMBE',
  'IMO',
  'JIGAWA',
  'KADUNA',
  'KANO',
  'KATSINA',
  'KEBBI',
  'KOGI',
  'KWARA',
  'LAGOS',
  'NASARAWA',
  'NIGER',
  'OGUN',
  'ONDO',
  'OSUN',
  'OYO',
  'PLATEAU',
  'RIVERS',
  'SOKOTO',
  'TARABA',
  'YOBE',
  'ZAMFARA',
];

// eslint-disable-next-line
const calculatePayrollPaymentsForState = (payroll_id, state) => {
  console.log(payroll_id);
  console.log(state);
  // Calculate total amount and total volunteers paid in state
  PayrollItem.aggregate(
    [
      {
        $match: {
          payroll_id: mongoose.Types.ObjectId(payroll_id),
          'volunteer_info.state': state,
          // status: 'paid'
        },
      },
      {
        $group: {
          _id: '$status',
          count: { $sum: 1 },
          amount: { $sum: '$amount_paid' },
          deductions: { $sum: '$deduction' },
        },
      },
    ],
    (err, obj) => {
      if (err) {
        console.log(err);
        return;
      }

      console.log(obj);
      // eslint-disable-next-line
      payrollStateStat = new PayrollStateStat();
      // eslint-disable-next-line
      payrollStateStat.payroll_id = payroll_id;
      // eslint-disable-next-line
      payrollStateStat.state = state;
      // eslint-disable-next-line
      payrollStateStat.deductions = 0;

      if (obj.length === 0) {
        console.log('no payroll info for this state paid');
        // eslint-disable-next-line
        payrollStateStat.amount = 0;
        // eslint-disable-next-line
        payrollStateStat.count = 0;
      } else {
        for (let i = 0; i < obj.length; i += 1) {
          // eslint-disable-next-line
          if (obj[i]._id === 'paid') {
            // eslint-disable-next-line
            payrollStateStat.amount = obj[i].amount;
            // eslint-disable-next-line
            payrollStateStat.count = obj[i].count;
            if (obj[i].deductions) {
              // eslint-disable-next-line
              payrollStateStat.deductions += obj[i].deductions;
            }
            // eslint-disable-next-line
          } else if (obj[i]._id === 'deceased') {
            // eslint-disable-next-line
            payrollStateStat.deceased = obj[i].count;
            if (obj[i].deductions) {
              // eslint-disable-next-line
              payrollStateStat.deductions += obj[i].deductions;
            }
            // eslint-disable-next-line
          } else if (obj[i]._id === 'hold') {
            // eslint-disable-next-line
            payrollStateStat.hold = obj[i].count;
            if (obj[i].deductions) {
              // eslint-disable-next-line
              payrollStateStat.deductions += obj[i].deductions;
            }
            // eslint-disable-next-line
          } else if (obj[i]._id === 'resigned') {
            // eslint-disable-next-line
            payrollStateStat.resigned = obj[i].count;
            if (obj[i].deductions) {
              // eslint-disable-next-line
              payrollStateStat.deductions += obj[i].deductions;
            }
            // eslint-disable-next-line
          } else if (obj[i]._id === 'exited') {
            // eslint-disable-next-line
            payrollStateStat.exited = obj[i].count;
            if (obj[i].deductions) {
              // eslint-disable-next-line
              payrollStateStat.deductions += obj[i].deductions;
            }
          }
        }
      }
      // eslint-disable-next-line
      payrollStateStat.save(err => {
        if (err) {
          console.log(err);
          return;
        }
        // eslint-disable-next-line
        console.log('Stage 3.3 complete process ended');
        // eslint-disable-next-line
        console.log(`Payroll stat for state saved successfully ${state}`);
      });
    },
  );
};

// eslint-disable-next-line
const calculatePayrollPaymentsForAllStates = payroll_id => {
  console.log('calculatePayrollPaymentsForAllStates called');
  console.log(payroll_id);
  // eslint-disable-next-line
  states_arr.forEach(element => {
    console.log(element);
    calculatePayrollPaymentsForState(payroll_id, element.toString());
  });
  // set key to let us know we've calculated for this month and year
  // redisClient.set(redisUtil.createRedisKey('PAYROLL%' + payroll_id + '#STATES'), 1);
};

// eslint-disable-next-line
const authorizePayroll = async (req, res) => {
  // eslint-disable-next-line
  const { payroll_id, admin } = req.body;
  try {
    const payroll = await Payroll.findById(payroll_id);
    // if payroll does not exist drop off
    if (payroll === undefined || payroll === null) {
      console.log('Payroll does not exist');
      return null;
    }

    if (payroll.status !== 'generated') {
      console.log('Error authorizing payroll. Payroll has already been generated');
      return null;
    }

    // HEAVY WORK DONE
    // eslint-disable-next-line
    let total_amount_paid = 0;
    // eslint-disable-next-line
    let total_deductions = 0;
    // eslint-disable-next-line
    let volunteers_paid = 0;
    // eslint-disable-next-line
    let volunteers_unpaid = 0;
    // eslint-disable-next-line
    let volunteers_male_paid = 0;
    // eslint-disable-next-line
    let volunteers_female_paid = 0;
    // eslint-disable-next-line
    let volunteers_male_unpaid = 0;
    // eslint-disable-next-line
    let volunteers_female_unpaid = 0;
    // eslint-disable-next-line
    let volunteers_unpaid_resigned = 0;
    // eslint-disable-next-line
    let volunteers_unpaid_deceased = 0;
    // eslint-disable-next-line
    let volunteers_unpaid_hold = 0;
    // eslint-disable-next-line
    let volunteers_unpaid_exited = 0;
    Volunteer.find({ registered: true }, (errr, volunteers) => {
      if (errr) {
        console.log(errr);
      }
      console.log(`starting number ${volunteers.length}`);
      volunteers.forEach(async (element) => {
        const payrollItem = {};

        const {
          _id,
          fname,
          mname,
          lname,
          state,
          gender,
          // eslint-disable-next-line
          centre_id,
          phone,
          email,
          nuban,
          // eslint-disable-next-line
          bank_name,
          bvn,
          status,
          // eslint-disable-next-line
          nibss_validated,
        } = element;
        // eslint-disable-next-line
        payrollItem.payroll_id = payroll._id;
        // eslint-disable-next-line
        payrollItem.volunteer = _id;
        payrollItem.volunteer_info = {};
        payrollItem.volunteer_info.firstname = fname;
        payrollItem.volunteer_info.middlename = mname;
        payrollItem.volunteer_info.surname = lname;
        payrollItem.volunteer_info.state = state;
        payrollItem.volunteer_info.gender = gender;
        // eslint-disable-next-line
        payrollItem.volunteer_info.center_id = centre_id;
        payrollItem.volunteer_info.phone = phone;
        payrollItem.volunteer_info.email = email;
        payrollItem.volunteer_info.nuban = nuban;
        // eslint-disable-next-line
        payrollItem.volunteer_info.bank_name = bank_name;
        payrollItem.volunteer_info.bvn = bvn;
        payrollItem.salary = 10000;

        if (
          status === 'enrolled'
          // eslint-disable-next-line
          && nibss_validated === true
        ) {
          payrollItem.deduction = 0;
          payrollItem.amount_paid = payrollItem.salary - payrollItem.deduction;
          payrollItem.status = 'paid';
          // eslint-disable-next-line
          total_amount_paid += payrollItem.amount_paid;
          // eslint-disable-next-line
          total_deductions += payrollItem.deduction;
          // eslint-disable-next-line
          volunteers_paid += 1;
          if (element.gender === 'M') {
            // eslint-disable-next-line
            volunteers_male_paid += 1;
          } else if (element.gender === 'F') {
            // eslint-disable-next-line
            volunteers_female_paid += 1;
          } else {
            // eslint-disable-next-line
            console.log(`Error for volunteer ${element._id} gender issues`);
          }
        } else {
          payrollItem.amount_paid = 0;
          // payrollItem.status = 'unpaid';
          payrollItem.status = status;
          // payrollItem.reason = volunteers[i].status;

          if (status === 'resigned') {
            // eslint-disable-next-line
            volunteers_unpaid_resigned += 1;
          } else if (status === 'hold') {
            // eslint-disable-next-line
            volunteers_unpaid_hold += 1;
          } else if (status === 'deceased') {
            // eslint-disable-next-line
            volunteers_unpaid_deceased += 1;
          } else if (status === 'exited') {
            // eslint-disable-next-line
            volunteers_unpaid_exited += 1;
          } else {
            // eslint-disable-next-line
            console.log(`unpaid volunteer ${element._id} has a problem somewhere`);
          }
          // eslint-disable-next-line
          volunteers_unpaid += 1;
          if (gender === 'M') {
            // eslint-disable-next-line
            volunteers_male_unpaid += 1;
          } else if (gender === 'F') {
            // eslint-disable-next-line
            volunteers_female_unpaid += 1;
          } else {
            // eslint-disable-next-line
            console.log(`unpaid volunteer ${element._id} has gender issues`);
          }
        }
        // eslint-disable-next-line
        const inside = await PayrollItem.create(payrollItem);
      });
      // Aggregate information into payroll
      // eslint-disable-next-line
      payroll.total_amount_paid = total_amount_paid;
      // eslint-disable-next-line
      payroll.total_deductions = total_deductions;
      // eslint-disable-next-line
      payroll.volunteers_paid = volunteers_paid;
      // eslint-disable-next-line
      payroll.volunteers_unpaid = volunteers_unpaid;
      // eslint-disable-next-line
      payroll.statistics.paid.gender.male = volunteers_male_paid;
      // eslint-disable-next-line
      payroll.statistics.paid.gender.female = volunteers_female_paid;
      // eslint-disable-next-line
      payroll.statistics.unpaid.gender.male = volunteers_male_unpaid;
      // eslint-disable-next-line
      payroll.statistics.unpaid.gender.female = volunteers_female_unpaid;
      // eslint-disable-next-line
      payroll.statistics.unpaid.status.resigned = volunteers_unpaid_resigned;
      // eslint-disable-next-line
      payroll.statistics.unpaid.status.deceased = volunteers_unpaid_deceased;
      // eslint-disable-next-line
      payroll.statistics.unpaid.status.hold = volunteers_unpaid_hold;
      // eslint-disable-next-line
      payroll.statistics.unpaid.status.exited = volunteers_unpaid_exited;

      payroll.generated.complete = true;

      return Payroll.findByIdAndUpdate(payroll_id, payroll, (errrr, pp) => {
        if (errrr) {
          console.log(errrr);
        }
        const savedPayroll = pp.toJSON();
        console.log('PAYROLL FINALLY & COMPLETELY SAVED AND GENERATED');
        console.log('stage 3.2 complete....');
        calculatePayrollPaymentsForAllStates(payroll_id);
        sms.send(admin.phone, `${savedPayroll.tag} successfully generated!`);
        Email.send(
          admin.email,
          `${savedPayroll.tag} Completed Successfully`,
          `${savedPayroll.tag} successfully generated!`,
        );
      });
    });
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  authorizePayroll,
};
