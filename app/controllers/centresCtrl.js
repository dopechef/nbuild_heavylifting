const bcrypt = require('bcrypt-nodejs');
const Excel = require('exceljs');
const NbuildCentre = require('../models/nbuildcentre');
const NtechCenter = require('../models/ntechcentre');
const CentreAdmin = require('../models/centreadmin');
const fs = require('fs');
const nodeCsv = require('csv');
//const mongoose = require('mongoose')
const regBodyHelper = require('./utils/regBodyHelper.utils');
const workbook = new Excel.Workbook();
// eslint-disable-next-line
const uploadCentreAdmin = async (req, res) => {
  try {
    
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
      // use workbook
      const inputPromises = []
      const updatePromises = []
      workbook.eachSheet((worksheet) => {
        worksheet.eachRow(async (row, rowNumber) => {
          // skip first row
          if (rowNumber !== 1) {
            // Prepare the Centre Admin
            // eslint-disable-next-line

            /**
             * find center admin using center id, if exists update else new
             */
            try{
              const admin = {}
              admin['fullname'] = row.values[3] != null ? row.values[3] : ''
              admin['mobile1'] = row.values[4] != null ? row.values[4] : ''
        
              admin['email'] = row.values[2]
              admin['first_login'] = true
              const centerAdmin = await CentreAdmin.findOne({centre_id: row.values[1]})
              admin['role'] = '599bd9f2053f3b7bc199aaa7'
              admin['first_login'] = true
              if(centerAdmin){//admin exists
                console.log('admin exists')
                /*admin['password'] = bcrypt.hashSync(`${row.values[5]}`)
                inputPromises.push(CentreAdmin.update({_id:centerAdmin._id}, admin))*/
              }else{//admin does not exist
                console.log('admin does not exist')
                const centre = await NbuildCentre.findOne({_id: row.values[1]})
                const STATE = centre.STATE != null ? centre.STATE : ''
                const LGA = centre.LGA != null ? centre.LGA.toUpperCase().trim() : ''
                if(centre){
                  admin['centre_id'] = row.values[1]
                  const unique = Math.floor(Math.random() * 1000 + 1);
                  admin['username'] = `${STATE}/${LGA}/${unique}`,
                  admin['password'] = bcrypt.hashSync(`${row.values[5]}`)
                  updatePromises.push(CentreAdmin.create(admin))
                }else{
                  console.log('center does not exist', row.values[1])
                  return
                }
              }
            }catch(e){
              console.log(e)
            }
          }
        });
      });
      const newAdmins = await Promise.all(inputPromises)
      const updateAdmins = await Promise.all(updatePromises)
      console.log(`newAdmins ${newAdmins.length}`, `updateAdmins ${updateAdmins.length}`)
      res.json('Done')
      return
    });
    console.log('done')
    return null;
  } catch (error) {
    console.log(error);
  }
};

const uploadCentres = async (req, res) => {
  try {
    //const workbook = new Excel.Workbook();
    workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
      // use workbook
      let id = 242
      workbook.eachSheet((worksheet) => {
        worksheet.eachRow(async (row, rowNumber) => {
          // skip first row
          if (rowNumber !== 1) {
            // Prepare the Centre Admin
            // eslint-disable-next-line
            const center = {}
            center['AGRIC TECHNOLOGY'] = row.values[1]
            center['AGRIC TECHNOLOGY_1'] = row.values[2]
            center['AGRIC TECHNOLOGY_2'] = row.values[3]

            center['AUTOMOBILE'] = row.values[4]
            center['AUTOMOBILE_1'] = row.values[5]
            center['AUTOMOBILE_2'] = row.values[6]

            center['CARPENTRY & JOINERY'] = row.values[8]
            center['CARPENTRY & JOINERY_1'] = row.values[9]
            center['CARPENTRY & JOINERY_2'] = row.values[10]
            
            center['CENTRE'] = row.values[11]

            center['ELECTRICAL'] = row.values[12]
            center['ELECTRICAL_1'] = row.values[13]
            center['ELECTRICAL_2'] = row.values[14]

            center['HOSPITALITY'] = row.values[15]
            center['HOSPITALITY_1'] = row.values[16]
            center['HOSPITALITY_2'] = row.values[17]

            center['LGA'] = row.values[18]

            center['MASONRY'] = row.values[19]
            center['MASONRY_1'] = row.values[20]
            center['MASONRY_2'] = row.values[21]

            center['PAINTING & DECORATING'] = row.values[22]
            center['PAINTING & DECORATING_1'] = row.values[23]
            center['PAINTING & DECORATING_2'] = row.values[24]

            center['PLUMBING'] = row.values[25]
            center['PLUMBING_1'] = row.values[26]
            center['PLUMBING_2'] = row.values[27]

            center['STATE'] = row.values[28]
            center['TRAINEES NC'] = row.values[29]

            center['WELDING'] = row.values[30]
            center['WELDING_1'] = row.values[31]
            center['WELDING_2'] = row.values[32]
            center['BATCH'] = 3
            center['status'] = row.values[33]
            center['id'] = ++id//row.values[34]

            NbuildCentre.create(center)
                        .then(s=>console.log(`${s._id} created`))
          }
        });
      });
    });
    console.log('done')
    return null;
  } catch (error) {
    console.log(error);
  }
};

const deleteAdmins = (req, res) => {
  const file = 'idsToDelete.csv'
  fs.readFile(file,'utf8', (err, content)=>{
    if(err){
      console.log(err)
      return
    }
    nodeCsv.parse(content, {columns: true, trim: true}, async (er, su)=>{
      if(er){
        console.log(er)
      }else{
        const a = []
        su.forEach(s=>{
          console.log(s)
          const id = s._id.replace(',','')
          a.push(CentreAdmin.deleteOne({_id:id}))
        })
        const deleted = await Promise.all(a)
        res.send(`done ${deleted.length}`)
        }
    })
  })
}

const addRegulatoryBodies = (req, res) => {
  workbook.xlsx.readFile(Buffer.from(req.file.path)).then(async () => {
    // use workbook
    const data = []
    workbook.eachSheet((worksheet) => {
      worksheet.eachRow(async (row, rowNumber) => {
        if (rowNumber !== 1) {
          const d = regBodyHelper(row, true)
          d.bodies.forEach(c=>{
            data.push(
              NbuildCentre.update(
                {_id:row.values[1]}, 
                {$addToSet:{regulatory_body:c}}
              )
            )
          })  
          /* data.push(
            NbuildCentre.findByIdAndUpdate(
              row.values[1], 
              {regulatory_body:[]}
            )
          )   */
          /* const c = await NbuildCentre.findById(row.values[1])
          if(c != null){
            c.regulatory_body = d.bodies
            data.push(c.save())
          } */
        }
      })
    })
    //console.log(data[0])
    try{
      const updateOp = await Promise.all(data)
      //await NbuildCentre.bulkWrite(data)
      res.json({
        success:true, 
        data:{
          records:data.length,
          updates: updateOp.modifiedCount
        }
      })
    }catch(e){
      const err = e.message ? e.message : e
      res.json({
        success:false,
        data:err
      })
    }
  })
}

const updateCentreQuota = async (req, res) => {
  
  try{
    const t = await workbook.xlsx.readFile(Buffer.from(req.file.path))
    const centersPromise = []
    workbook.eachSheet((worksheet) => {
      worksheet.eachRow((row, rowNumber) => {
        if (rowNumber !== 1) {
          const id = row.values[1]
          const dataQuery = {}
          dataQuery['AUTOMOBILE_2'] = row.values[2]
          dataQuery['PAINTING & DECORATING_2'] = 0
          dataQuery['WELDING_2'] = 0
          dataQuery['AGRIC TECHNOLOGY'] = 0
          dataQuery['AGRIC TECHNOLOGY_1'] = 0
          dataQuery['AGRIC TECHNOLOGY_2'] = row.values[3] 
          dataQuery['CARPENTRY & JOINERY_2'] = row.values[4] 
          dataQuery['ELECTRICAL_2'] = row.values[5]
          dataQuery['MASONRY_2'] = row.values[6]
          dataQuery['PLUMBING_2'] = row.values[7]
          dataQuery['HOSPITALITY'] = 0
          dataQuery['HOSPITALITY_1'] = 0
          dataQuery['HOSPITALITY_2'] = row.values[8]
          centersPromise.push(
            NbuildCentre.findByIdAndUpdate(id, dataQuery)
          )
          /*if(row.values[2]){
            centersPromise.push(
              NtechCenter.findByIdAndUpdate(
                row.values[2], {capacity:row.values[4]}
              )
            );
          }*/
        }
      })
    })
    const centers = await Promise.all(centersPromise)
    console.log(centers)
    res.json({
      success:true,
      message:`updates completed for ${centers.length} centers`
    })
  }catch(e){
    res.json({
      success:false,
      data:e
    })
  }
}

const updateBatch = async (req, res) => {
  try{
    const t = await workbook.xlsx.readFile(Buffer.from(req.file.path))
    const centersPromise = []
    workbook.eachSheet((worksheet) => {
      worksheet.eachRow(async (row, rowNumber) => {
        if (rowNumber !== 1) {
          //const updateOp = await 
          centersPromise.push(NbuildCentre.updateMany(
            { _id:row.values[1] }, 
            //{ $set:{BATCH:[]} }
            { $addToSet:{ BATCH:3} }
          ))
        }
      })
    })
    const result = await Promise.all(centersPromise)
    res.json({
      success:true,
      message:`Done ${centersPromise.length}`,
      data:result
    }) 
  }catch(e){
    console.log(e)
    res.json({
      success:false,
      data:e
    })
  }
}

module.exports = {
  uploadCentreAdmin,
  uploadCentres,
  deleteAdmins,
  addRegulatoryBodies,
  updateCentreQuota,
  updateBatch
};
